<div class="modal fade" id="delete<?php echo $r->id_detailkelas;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
		
			<!-- Header -->
			<div class="modal-header btn-danger">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Remove Siswa<br>
					<?php echo $kelas." (".$cur_tahun.")";?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				Anda yakin ingin meremove <br>
				(<?php echo $r->nama?>) <br>
				dari kelas (<?php echo $kelas;?>)?
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php
				$hidden = array('id'=>$r->id_detailkelas,'id_kelas'=>$datakelas->row('id_kelas'));
				echo form_open('master/kelas/','',$hidden);
					echo form_submit('remove','Remove','class="btn btn-danger"');
				?>	
				</form>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		
	</div>
</div>