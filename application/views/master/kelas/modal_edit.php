<div class="modal fade" id="editWali" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<?php
			$hidden = array('id_kelas'=>$id_masterkelas);
			echo form_open('master/kelas/','',$hidden); 
			?>
			
			<!-- Header -->
			<div class="modal-header btn-info">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Wali Kelas<br>
					<?php echo $kelas." (".$cur_tahun.")";?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				<div class="form-group">
					<label>Wali Kelas <span class="asterik">*</span> :</label>
					<?php echo combobox('id_guru','master_guru','','nama','id_guru','','nama asc',$id_wali_kelas); ?>	
				</div>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php echo form_submit('edit_wali','Update','class="btn btn-info"'); ?>				
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
			
			</form>
		</div>
		
	</div>
</div>