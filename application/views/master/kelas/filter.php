<!-- Panel Filter -->
<div class="col-md-3">
<div class="panel panel-custom">
	<div class="panel-heading">
		Menu Filter
	</div>
	<div class="panel-body">
		<?php echo form_open('master/kelas/');?>
		
		<!-- Kelas -->
		<div class="form-group">
			<label>Kelas :</label>
			<?php echo combokelas('kelas','app_kelas','','tingkat_kelas','kelas','id_datakelas','',$cur_idkelas);?>
		</div>
		
		<!-- Tahun ajar -->
		<div class="form-group">
			<label>Tahun Ajaran :</label>
			<?php echo combobox('tahun_ajar','akademik_tahun_ajar','','tahun_ajar','id_tahun_ajar','','tahun_ajar desc',$cur_idtahun);?>
		</div>		
		
		<!-- Submit -->
		<div class="form-group">
			<button name="session_filter" type="submit" class="btn btn-custom"><i class="fa fa-search"></i> Lihat</button>
		</div>
		
		<?php echo form_close();?>
	</div>
</div>
</div><!-- /.Panel Filter -->