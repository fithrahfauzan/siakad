<div class="modal fade" id="add" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<?php 
			$hidden = array('id_kelas'=>$id_masterkelas);
			echo form_open('master/kelas/','id="checkboxSubmitBtn"',$hidden); 
			?>
			
			<!-- Header -->
			<div class="modal-header btn-custom">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Input Siswa - <?php echo $kelas." (".$cur_tahun.")";?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">				
				<!-- datatables -->
				<table id="tableCheckBox" class="table table-striped table-hover style-table" width="100%">
					<thead>
						<tr>
							<th></th>
							<th>Nis</th>
							<th>Nama</th>
							<th>Jenis Kelamin</th>						
						</tr>
					</thead>
					<tbody>
					<?php 
					foreach ($siswa as $s)
					{
						?>
						<tr>
							<td align="center"><?php echo $s->id_siswa;?></td>
							<td><?php echo $s->nis; ?></td>
							<td><?php echo $s->nama; ?></td>
							<td align="center"><?php echo jns_kelamin($s->jns_kelamin); ?></td>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<button type="submit" name="post_siswa" class="btn btn-custom">Simpan</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				
				<!--<p><b>Selected rows data:</b></p>
				<pre id="example-console-rows"></pre>-->		
			</div>
			
			</form>	
		</div>
		
	</div>
</div>

<!-- datatables jquery -->
<script src="<?php echo base_url() ?>assets/js/jquery-1.12.3.min.js"></script>
<script>
$(document).ready(function (){
	var table = $('#tableCheckBox').DataTable({
		'columnDefs': [{
			'targets': 0,
			'checkboxes': {
			   'selectRow': true
			}
		}],
		'select': {
			'style': 'multi'
		},
		'order': [[1, 'asc']]
	});
	
	// Handle form submission event 
	$('#checkboxSubmitBtn').on('submit', function(e){
	  var form = this;	  
	  var rows_selected = table.column(0).checkboxes.selected();

	  // Iterate over all selected checkboxes
	  $.each(rows_selected, function(index, rowId){
		 // Create a hidden element 
		 $(form).append(
			 $('<input>')
				.attr('type', 'hidden')
				.attr('name', 'id_siswa[]')
				.val(rowId)
		 );
	  });
	  
	  /** // Output form data to a console     
	  $('#example-console-rows').text(rows_selected.join(","));	  
	   
	  // Remove added elements
	  $('input[name="id\[\]"]', form).remove();
	   
	  // Prevent actual form submission
	  e.preventDefault();**/
	});   
});
</script>