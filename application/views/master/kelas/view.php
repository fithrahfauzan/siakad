<div class="row">

<?php 
$this->load->view('master/kelas/filter');

if($this->uri->segment(3)=="filter"){
?>

<!-- content -->
<div class="col-md-9">
<?php
// Message
if($this->session->flashdata('success') != ""){
	echo alert_c("save_success");
}
if($this->session->flashdata('warning') != ""){
	echo alert_c("warning");
}
if($this->session->flashdata('remove') != ""){
	echo alert_c("remove_success");
}
?>
<div class="panel panel-custom">

	<div class="panel-heading">
		Data Kelas #<?php echo $kelas;?> (Tahun Ajaran <?php echo $cur_tahun;?>)
		
		<?php 
		// cetak
		if($checkkelasdb->num_rows()!=0){
			$hidden = array('id_kelas'=>$id_masterkelas);
			echo form_open('master/kelas/','class="pull-right" target="_blank"',$hidden); 	
			?>
			<button type="submit" name="cetak" class="btn btn-xs btn-default">
				Cetak Data Kelas
			</button>			
		</form>
		<?php } ?>
	</div>

	<div class="panel-body">
	<?php
	// check master_kelas db
	if($checkkelasdb->num_rows()==0){
		?>
		<div class="row">
			<div class="col-md-12 text-center">
				<?php
				$hidden = array('generate'=>'1','kelas'=>$cur_idkelas,'tahun_ajar'=>$cur_idtahun);
				echo form_open_multipart('master/kelas/','',$hidden);
				?>
					Data belum terkonfigurasi, silahkan klik
					<button type="submit" name="session_filter" class="btn btn-info">Generate Data</button>
				<?php
				form_close();
				?>
			</div>			
		</div>
		<?php
	}
	
	// tampil detail master_kelas
	else{
	?>
		<div class="row">
			<div class="col-md-6" style="font-size:17">
				<!-- wali kelas -->
				Wali Kelas : 
				<?php 
				if($wali_kelas != ""){
					echo $wali_kelas; 
				}
				else{
					echo "Belum ada"; 
				}
				?>
				<!-- edit wali kelas -->
				<button data-toggle="modal" data-target="#editWali" 
				rel="tooltip" data-trigger="hover" title="Edit Wali Kelas"
				class="btn btn-xs btn-info">
					<i class="fa fa-pencil"></i>
				</button>
				<?php include "modal_edit.php";?>							
			</div>
			<div class="col-md-6 text-right" style="font-size:17">
				<!-- jumlah siswa -->
				Jumlah Siswa : <?php echo $jumlah; ?> siswa<br>
				Laki - laki : <?php if($jumlahL=="")echo "0"; else echo $jumlahL; ?> siswa<br>
				Perempuan : <?php  if($jumlahP=="")echo "0"; else echo $jumlahP; ?> siswa<br>
			</div>
		</div>
		
		<br>
		<table class="table table-striped table-hover style-table">
			<thead>
				<tr>
					<th>No</th>
					<th>Nis</th>
					<th>Nama</th>
					<th>Jenis Kelamin</th>
					<th class="t-action"></th>
				</tr>
			</thead>
			<tbody>
			<?php
			if($datakelas->num_rows()>0){
				$i = 1;
				foreach($datakelas->result() as $r){
				?>
					<tr>
						<td align="center"><?php echo $i;?></td>
						<td align="center"><?php echo $r->nis;?></td>
						<td><?php echo $r->nama;?></td>
						<td align="center"><?php echo jns_kelamin($r->jns_kelamin);?></td>
						<td align="center">
							<!-- remove -->
							<button data-toggle="modal" data-target="#delete<?php echo $r->id_detailkelas;?>"
							rel="tooltip" data-trigger="hover" title="Remove" 
							class="btn btn-xs btn-danger">
								<i class="fa fa-times"></i>
							</button>
						</td>
					</tr>
					<?php
					include "modal_delete.php";
					$i++;
				}
			}
			// belum ada siswa
			else{
				?>
				<tr><td colspan="5" align="center">Belum ada data</td></tr>
				<?php
			}
			?>
			</tbody>
		</table>

		<button data-toggle="modal" data-target="#add" title="Input Siswa" class="btn btn-custom pull-right" style="margin-top:10px;">
			<i class="fa fa-plus"></i> Input Siswa
		</button>
		
	<?php
	}
	?>
	</div>

</div>
</div><!-- /.content -->

	<?php 
	include "modal_post.php";

}
?>

</div><!-- /.row -->
