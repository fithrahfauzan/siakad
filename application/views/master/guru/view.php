<?php echo anchor('master/guru/post','<i class="fa fa-plus"></i> Input Data','class="btn btn-custom btn-no-radius-bottom"');?>

<div class="panel panel-default panel-no-radius-top">
<div class="panel-body">
<?php
// Message
if($this->session->flashdata('warning') != ""){
	echo alert_c("save_tanpa_foto");
}
if($this->session->flashdata('success') != ""){
	echo alert_c("save_success");
}
if($this->session->flashdata('delete') != ""){
	echo alert_c("delete_success");
}
?>
<table id="datatables" class="table table-striped table-hover style-table">
	<thead>
		<tr>
			<th class="defaultSort sortDesc">ID</th>
			<th>Kode Guru</th>
			<th>Nip</th>
			<th>Nama</th>
			<th>Jenis Kelamin</th>			
			<th>Pendidikan Umum</th>
			<th class="t-action"></th>
		</tr>
	</thead>
	<tbody>
	<?php 
	foreach ($record as $r)
	{
	?>
		<tr>
			<td align="center"><?php echo $r->id_guru;?></td>
			<td align="center"><?php echo $r->kode_guru;?></td>
			<td><?php if($r->nip != ""){echo $r->nip;} else{echo "-";} ?></td>
			<td><?php echo $r->nama; ?></td>
			<td><?php echo jns_kelamin($r->jns_kelamin); ?></td>
			<td><?php echo $r->matpel; ?></td>			
			<td align="center">
				<!-- detail -->
				<button data-toggle="modal" data-target="#detail<?php echo $r->id_guru;?>"
				rel="tooltip" data-trigger="hover" title="Detail" 
				class="btn btn-xs btn-primary">
					<i class="fa fa-search"></i>
				</button>
				
				<!-- edit -->
				<a href="<?php echo base_url().'master/guru/edit/'.$r->id_guru;?>" 
				rel="tooltip" data-trigger="hover" title="Edit" 
				class="btn btn-xs btn-info">
					<i class="fa fa-pencil"></i>
				</a>
				
				<!-- delete -->
				<button data-toggle="modal" data-target="#delete<?php echo $r->id_guru;?>"
				rel="tooltip" data-trigger="hover" title="Delete" 
				class="btn btn-xs btn-danger">
					<i class="fa fa-times"></i>
				</button>
			</td>
		</tr>
		
		<!-- Modal Delete -->
		<div class="modal fade" id="delete<?php echo $r->id_guru;?>" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
				
					<!-- Header -->
					<div class="modal-header btn-danger">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Delete Data Guru #<?php echo $r->kode_guru;?></h4>
					</div>
					<!-- Content -->
					<div class="modal-body">
						Anda yakin ingin menghapus?<br>
						Data yang berkaitan dengan guru ini akan terhapus.
					</div>
					<!-- Footer -->
					<div class="modal-footer">
						<?php
						$hidden = array('id'=>$r->id_guru);
						echo form_open('master/guru/','',$hidden);
							echo form_submit('delete','Delete','class="btn btn-danger"');
						?>	
						</form>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
				
			</div>
		</div><!-- ./End Modal -->
		
		
		<!-- Modal Detail -->
		<div class="modal fade modal-custom" id="detail<?php echo $r->id_guru;?>" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
				
					<!-- Header -->
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Data Guru #<?php echo $r->kode_guru;?></h4>
					</div>

					<!-- Content -->
					<div class="modal-body">
						<div class="row row-no-margin">
							<div class="m-foto col-md-3">
								<img src="<?php 
									if($r->foto != ""){
										echo base_url().'assets/img/foto/guru/'.$r->foto;
									}
									else{
										echo base_url().'assets/img/foto/noprofile.gif';
									}
								?>" width="100%"/>
							</div>
							<div class="m-profile col-md-9">
								<div class="row">
									<!-- NIP -->
									<div class="col-md-6">
										<strong>NIP :</strong>
										<div class="well well-sm">
											<?php if($r->nip != ""){echo $r->nip;} else{echo "-";}?>
										</div>
									</div>
									
									<!-- pddkn_umum -->
									<div class="col-md-6">
										<strong>Pendidikan Umum :</strong>
										<div class="well well-sm"><?php echo $r->matpel;?></div>
									</div>
								</div>
								
								<!-- Nama -->
								<div class="row">
									<div class="col-md-12">
										<strong>Nama Lengkap :</strong>
										<div class="well well-sm"><?php echo $r->nama;?></div>
									</div>
								</div>
								
								<!-- TTL -->
								<div class="row">
									<div class="col-md-12">
										<strong>Tempat / Tanggal Lahir :</strong>
										<div class="row">
											<div class="col-md-6">
												<div class="well well-sm">
													<?php echo $r->tempat_lahir;?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="well well-sm">
													<?php echo $r->tanggal_lahir;?>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<!-- Jns_kelamin -->
									<div class="col-md-5">
										<strong>Jenis Kelamin :</strong>
										<div class="well well-sm"><?php echo jns_kelamin($r->jns_kelamin);?></div>
									</div>
									
									<!-- Agama -->
									<div class="col-md-4">
										<strong>Agama :</strong>
										<div class="well well-sm"><?php echo $r->agama;?></div>
									</div>
									
									<!-- Gol Darah -->
									<div class="col-md-3">
										<strong>Gol Darah :</strong>
										<div class="well well-sm"><?php echo $r->gol_darah;?></div>
									</div>
								</div>
								
								<!-- Alamat -->
								<div class="row">
									<div class="col-md-12">
										<strong>Alamat :</strong>
										<div class="well well-sm">
										<div class="row">
										
											<!-- Jalan -->
											<div class="col-md-12">
												<h4>Nama Jalan</h4>
												<div class="well well-sm well-custom">
													<?php echo alamat('jalan',$r->alamat);?>
												</div>
											</div>
											
											<!-- RT/RW -->
											<div class="col-md-2">
												<h4>RT</h4>
												<div class="well well-sm well-custom">
													<?php echo alamat('rt',$r->alamat);?>
												</div>
											</div>
											
											<!-- RW -->
											<div class="col-md-2">
												<h4>RW</h4>
												<div class="well well-sm well-custom">
													<?php echo alamat('rw',$r->alamat);?>
												</div>
											</div>
											
											<!-- Nomor -->
											<div class="col-md-2">
												<h4>Nomor</h4>
												<div class="well well-sm well-custom">
													<?php echo alamat('nomor',$r->alamat);?>
												</div>
											</div>
											
											<!-- Kel -->
											<div class="col-md-6">
												<h4>Kelurahan</h4>
												<div class="well well-sm well-custom">
													<?php echo alamat('kel',$r->alamat);?>
												</div>
											</div>
											
											<!-- Kec -->
											<div class="col-md-4">
												<h4>Kecamatan</h4>
												<div class="well well-sm well-custom">
													<?php echo alamat('kec',$r->alamat);?>
												</div>
											</div>
											
											<!-- Kab-->
											<div class="col-md-4">
												<h4>Kabupaten</h4>
												<div class="well well-sm well-custom">
													<?php echo alamat('kab',$r->alamat);?>
												</div>
											</div>
											
											<!-- Prov -->
											<div class="col-md-4">
												<h4>Provinsi</h4>
												<div class="well well-sm well-custom">
													<?php echo alamat('prov',$r->alamat);?>
												</div>
											</div>
										</div>	
										</div>
									</div>
								</div><!-- /.Alamat -->
								
								<div class="row">
									<!-- Email -->
									<div class="col-md-6">
										<strong>Email :</strong>
										<div class="well well-sm">
											<?php if($r->email==""){echo"-";} echo $r->email;?>
										</div>
									</div>
									
									<!-- Telepon -->
									<div class="col-md-6">
										<strong>Telepon :</strong>
										<div class="well well-sm">
											<?php if($r->telp==""){echo"-";} echo $r->telp;?>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div><!-- /.content -->

					<!-- Footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
				
			</div>
		</div><!-- ./End Modal -->
		
		<?php
	}
	?>
	</tbody>
</table>
</div>
</div>