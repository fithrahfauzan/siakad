<?php
echo anchor('master/guru/','Kembali','class="btn btn-custom btn-no-radius-bottom"');

echo form_open_multipart('master/guru/post');
?>

<div class="panel panel-default panel-no-radius-top">
	<div class="panel-body">
	<?php
	// Message
	if($this->session->flashdata('error') != ""){
		echo alert_c("foto_error");
	}
	if($this->session->flashdata('warning') != ""){
		echo alert_c("save_tanpa_foto");
	}
	if($this->session->flashdata('success') != ""){
		echo alert_c("save_success");
	}
	if($this->session->flashdata('duplicate') != ""){
		echo '
			<div class="alert alert-danger fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Error : NIP yang diinput telah terdaftar.
			</div>
		';
	}
	?>
	<div class="row">
		<!-- Foto -->
		<div class="m-foto col-md-3">
			<strong>Foto :</strong>
			<img src="<?php echo base_url().'assets/img/foto/noprofile.gif';?>" width="100%"/>
			<?php echo form_upload('foto','','class="form-control" accept="image/*"');?>
			<div class="well well-sm well-no-radius">
				<table class="table table-no-margin-bot">
					<tr>
						<td>Format</td>
						<td>jpeg/jpg</td>
					</tr>
					<tr>
						<td>Max Size</td>
						<td>3 mb</td>
					</tr>
				</table>
			</div>
		</div>
		
		<div class="m-profile m-edit col-md-8 col-md-offset-1">
			<div class="row">
				<!-- NIP -->
				<div class="col-md-4">
					<strong>NIP :</strong>
					<?php echo form_input('nip', '', 'class="form-control" maxlength="50" placeholder="NIP ..."');?>
				</div>
				
				<!-- pddkn_umum -->
				<div class="col-md-8">
					<strong>Pendidikan Umum <span class="asterik">*</span> :</strong>
					<?php echo combobox('pddkn_umum','akademik_matpel','','matpel','id_matpel','','matpel',''); ?>
				</div>
			</div>
			
			<!-- Nama -->
			<div class="row">
				<div class="col-md-12">
					<strong>Nama Lengkap <span class="asterik">*</span> :</strong>
					<?php echo form_input('nama', '', 'class="form-control" maxlength="50" required placeholder="Nama lengkap ..."' );?>
				</div>
			</div>
			
			<!-- TTL -->
			<div class="row">
				<div class="col-md-12">
					<strong>Tempat / Tanggal Lahir <span class="asterik">*</span> :</strong>
					<div class="row">
						<div class="col-md-6">
							<?php echo form_input('tempat_lahir', '', 'class="form-control" maxlength="30" required placeholder="Tempat Lahir ..."' );?>
						</div>
						<div class="col-md-4">
							<div class='input-group'>
								<?php echo form_input('tanggal_lahir', '', 'class="form-control" id="datetimepicker1" required placeholder="Tanggal Lahir ..."' );?>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<!-- Jns_kelamin -->
				<div class="col-md-5">
					<strong>Jenis Kelamin <span class="asterik">*</span> :</strong>
					<?php echo form_dropdown('jns_kelamin',array("L"=>jns_kelamin("L"),"P"=>jns_kelamin("P")),'L', 'class="form-control"')?>
				</div>
				
				<!-- Agama -->
				<div class="col-md-4">
					<strong>Agama <span class="asterik">*</span> :</strong>
					<?php echo combobox('agama','app_agama','','agama','id_agama','','',''); ?>
				</div>
				
				<!-- Gol Darah -->
				<div class="col-md-3">
					<strong>Gol Darah :</strong>
					<?php echo form_dropdown('gol_darah',gol_darah(),'Not Set', 'class="form-control"')?>
				</div>
			</div>
			
			<!-- Alamat -->
			<div class="row">
				<div class="col-md-12">
					<strong>Alamat :</strong>
					<div class="well well-sm well-no-margin">
					<div class="row">
					
						<!-- Jalan -->
						<div class="col-md-12">
							<h4>Nama Jalan</h4>
							<?php echo form_input('jalan', '', 'class="form-control" maxlength="50" placeholder="Nama Jalan ..."' );?>
						</div>
						
						<!-- RT/RW -->
						<div class="col-md-2">
							<h4>RT</h4>
							<?php echo form_input('rt', '', 'class="form-control" maxlength="4" placeholder="RT ..."' );?>
						</div>
						
						<!-- RW -->
						<div class="col-md-2">
							<h4>RW</h4>
							<?php echo form_input('rw', '', 'class="form-control" maxlength="4" placeholder="RW ..."' );?>
						</div>
						
						<!-- Nomor -->
						<div class="col-md-2">
							<h4>Nomor</h4>
							<?php echo form_input('nomor', '', 'class="form-control" maxlength="5" placeholder="No ..."' );?>
						</div>
						
						<!-- Kel -->
						<div class="col-md-6">
							<h4>Kelurahan</h4>
							<?php echo form_input('kel', '', 'class="form-control" maxlength="30" placeholder="Kelurahan ..."' );?>
						</div>
						
						<!-- Kec -->
						<div class="col-md-4">
							<h4>Kecamatan</h4>
							<?php echo form_input('kec', '', 'class="form-control" maxlength="30" placeholder="Kecamatan ..."' );?>
						</div>
						
						<!-- Kab-->
						<div class="col-md-4">
							<h4>Kabupaten</h4>
							<?php echo form_input('kab', '', 'class="form-control" maxlength="30" placeholder="Kabupaten ..."' );?>
						</div>
						
						<!-- Prov -->
						<div class="col-md-4">
							<h4>Provinsi</h4>
							<?php echo form_input('prov', '', 'class="form-control" maxlength="30" placeholder="Provinsi ..."' );?>
						</div>
						
					</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<!-- Email -->
				<div class="col-md-6">
					<strong>Email :</strong>
					<?php echo form_input('email', '', 'class="form-control" maxlength="40" placeholder="Email ..."' );?>
				</div>
				
				<!-- Telepon -->
				<div class="col-md-6">
					<strong>Telepon :</strong>
					<?php echo form_input('telp', '', 'class="form-control" maxlength="28" placeholder="Telepon ..."' );?>
				</div>
			</div>
			
			<div class="row">
				<div class="checkbox col-md-3">
					<label><input name="reinput" type="checkbox" value="1" class="form_control"> Input Lagi</label>
				</div>
			</div>
			
			<?php echo form_submit('submit','Simpan','class="btn btn-custom"');?>
			<?php echo form_reset('','Reset','class="btn btn-success"');?>
			
		</div>
	</div>
	</div>
</div>
<?php echo form_close();?>