<div class="modal fade" id="post_pelajaran<?php echo $k->id_jadwal;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<?php 
			$hidden = array('id_kelas'=>$id_kelas,'id_jadwal'=>$k->id_jadwal);
			echo form_open('akademik/jadwal/','',$hidden); 
			?>
			
			<!-- Header -->
			<div class="modal-header btn-custom">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Input Jadwal Pelajaran<br>
					Hari <?php echo tampil_hari($k->hari);?> - Kelas <?php echo $kelas;?><br>
					<?php echo $cur_tahun;?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				<!-- Waktu -->
				<div class="form-group">
					<label>Waktu <span class="asterik">*</span> :</label>
					<?php echo form_input('waktu', '', 'class="form-control" maxlength="50" required placeholder="Waktu ..."');?>
					<p class="help-block">Format: "00:00 - 00:00"</p>
				</div>
				<!-- Matpel -->
				<div class="form-group">
					<label>Mata Pelajaran <span class="asterik">*</span> :</label>
					<?php echo combobox('matpel','akademik_matpel','','matpel','id_matpel','','matpel',''); ?>
				</div>
				<!-- Pengajar -->
				<div class="form-group">
					<label>Pengajar <span class="asterik">*</span> :</label>
					<?php echo combopengajar('pengajar','master_guru','akademik_matpel','',''); ?>
				</div>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php echo form_submit('post_pelajaran','Simpan','class="btn btn-custom"');?>	
				</form>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		
	</div>
</div>