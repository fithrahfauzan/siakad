<div class="modal fade" id="delete_jadwal<?php echo $r->id_jadwal_detail;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
		
			<!-- Header -->
			<div class="modal-header btn-danger">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Delete Jadwal ID #<?php echo $r->id_jadwal_detail;?><br>
					(<?php echo $r->waktu;?>)<br>
					Hari <?php echo tampil_hari($k->hari);?> - Kelas <?php echo $kelas;?><br>
					<?php echo $cur_tahun;?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				Anda yakin ingin menghapus?
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php
				$hidden = array('id'=>$r->id_jadwal_detail,'id_kelas'=>$id_kelas,'id_jadwal'=>$k->id_jadwal);
				echo form_open('akademik/jadwal/','',$hidden);
					echo form_submit('delete_jadwal','Delete','class="btn btn-danger"');
				?>					
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</form>
			</div>
		</div>
		
	</div>
</div>