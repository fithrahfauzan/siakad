<div class="modal fade" id="delete_hari<?php echo $k->id_jadwal;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
		
			<!-- Header -->
			<div class="modal-header btn-danger">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Delete Hari ID #<?php echo $k->id_jadwal;?> (<?php echo tampil_hari($k->hari);?>)<br>
					Kelas <?php echo $kelas;?><br>
					<?php echo $cur_tahun;?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				Anda yakin ingin menghapus?<br>
				Data yang berkaitan akan terhapus.
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php
				$hidden = array('id_kelas'=>$id_kelas,'id_jadwal'=>$k->id_jadwal);
				echo form_open('akademik/jadwal/','',$hidden);
					echo form_submit('delete_hari','Delete','class="btn btn-danger"');
				?>					
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</form>
			</div>
		</div>
		
	</div>
</div>