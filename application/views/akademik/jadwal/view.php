<div class="row">

<?php 
$this->load->view('akademik/jadwal/filter');

if($this->uri->segment(3)=="filter"){
?>

<!-- content -->
<div class="col-md-9">
<?php
// Message
if($this->session->flashdata('success') != ""){
	echo alert_c("save_success");
}
if($this->session->flashdata('duplicate') != ""){
	echo alert_c("duplicate");
}
if($this->session->flashdata('delete') != ""){
	echo alert_c("delete_success");
}
?>
<div class="panel panel-custom">

	<div class="panel-heading">
		Jadwal Kelas #<?php echo $kelas;?> (Tahun Ajaran <?php echo $cur_tahun;?>)
		
		<?php 
		// cetak
		if($checkkelasdb->num_rows()!=0){
			$hidden = array('id_kelas'=>$id_kelas);
			echo form_open('akademik/jadwal/','class="pull-right" target="_blank"',$hidden); 	
			?>
			<button type="submit" name="cetak" class="btn btn-xs btn-default">
				Cetak Jadwal Pelajaran
			</button>			
		</form>
		<?php } ?>
	</div>

	<div class="panel-body">
	<?php
	// check master_kelas db
	if($checkkelasdb->num_rows()==0){
		?>
		<div class="row">
			<div class="col-md-12 text-center">
				<?php
				$hidden = array('generate'=>'1','kelas'=>$cur_idkelas,'tahun_ajar'=>$cur_idtahun);
				echo form_open_multipart('akademik/jadwal/','',$hidden);
				?>
					Data Kelas belum terkonfigurasi, silahkan klik
					<button type="submit" name="session_filter" class="btn btn-info">Generate Data</button>
				<?php
				form_close();
				?>
			</div>			
		</div>
		<?php
	}
	
	// tampil detail jadwal
	else{
	?>
		<div class="row">
			<!-- Hari Accordion -->
			<div class="panel-group col-md-12" id="accordion">
			<?php	
			foreach($hari as $k){
				?>
				<div class="panel panel-custom">
				<div class="panel-heading">
					<h4 class="panel-title">
						<!-- nama hari -->
						<a data-toggle="collapse" data-parent="#accordion" 
						href="#collapse<?php echo $k->id_jadwal;?>" 
						aria-expanded="true" class="a-hover-no-underline">
							<i class="fa fa-arrows-v"></i> <?php echo tampil_hari($k->hari);?>
						</a>
						<!-- delete hari -->
						<button data-toggle="modal" 
						data-target="#delete_hari<?php echo $k->id_jadwal;?>"
						rel="tooltip" data-trigger="hover" title="Delete Hari" 
						class="btn btn-xs btn-warning pull-right">
							<i class="fa fa-times"></i>
						</button>
					</h4>
				</div>
				<div id="collapse<?php echo $k->id_jadwal;?>"
				class="panel-collapse collapse 
				<?php if($this->session->flashdata('cur_hari') == $k->id_jadwal){echo "in";} ?>" 
				aria-expanded="<?php if($this->session->flashdata('cur_hari') == $k->id_jadwal){echo "true";}else{echo"false";} ?>">
					<div class="panel-body">
					<!-- Tabel Jadwal -->
					<table class="table table-striped table-hover style-table">
						<thead>
							<tr>
								<th>Jam Ke</th>
								<th>Waktu</th>
								<th>Pelajaran</th>								
								<th>Pengajar</th>
								<th class="t-action"></th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($jadwal[$k->id_jadwal]->num_rows()>=1){
							$i = 0;
							foreach($jadwal[$k->id_jadwal]->result() as $r){
								// kegiatan lain
								if($r->kegiatan != ""){
								?>
									<tr class="highlight">
										<td></td>
										<td align="center"><?php echo $r->waktu;?></td>
										<td align="center" colspan="2" class="text-uppercase">
											<strong><?php echo $r->kegiatan;?></strong>
										</td>
										<td align="center">
											<!-- edit -->
											<button data-toggle="modal" 
											data-target="#edit_kegiatan<?php echo $r->id_jadwal_detail;?>"
											rel="tooltip" data-trigger="hover" title="Edit" 
											class="btn btn-xs btn-info">
												<i class="fa fa-pencil"></i>
											</button>
											<!-- delete -->
											<button data-toggle="modal"
											data-target="#delete_jadwal<?php echo $r->id_jadwal_detail;?>"
											rel="tooltip" data-trigger="hover" title="Delete" 
											class="btn btn-xs btn-danger">
												<i class="fa fa-times"></i>
											</button>
										</td>
									</tr>
									<?php
									// modal edit
									include "modal_edit_kegiatan.php";
								}
								// kbm
								else{
									$i++;
									?>
									<tr>
										<td align="center"><?php echo $i;?></td>
										<td align="center"><?php echo $r->waktu;?></td>
										<td align="center"><?php echo $r->matpel;?></td>
										<td align="center"><?php echo $r->pengajar;?></td>
										<td align="center">
											<!-- edit -->
											<button data-toggle="modal" 
											data-target="#edit_pelajaran<?php echo $r->id_jadwal_detail;?>"
											rel="tooltip" data-trigger="hover" title="Edit" 
											class="btn btn-xs btn-info">
												<i class="fa fa-pencil"></i>
											</button>
											<!-- delete -->
											<button data-toggle="modal"
											data-target="#delete_jadwal<?php echo $r->id_jadwal_detail;?>"
											rel="tooltip" data-trigger="hover" title="Delete" 
											class="btn btn-xs btn-danger">
												<i class="fa fa-times"></i>
											</button>
										</td>
									</tr>
									<?php
									// modal edit
									include "modal_edit_pelajaran.php";
								}
								// modal delete jadwal
								include "modal_delete_jadwal.php";

							}
						}
						// belum  ada data
						else{
							?>
							<tr><td colspan="5" align="center">Belum ada data</td></tr>
							<?php
						}
						?>
						</tbody>
					</table>
					
					<!-- Post Jadwal -->
					<div class="row"><div class="col-md-12 text-right">
						<button data-toggle="modal" 
						data-target="#post_pelajaran<?php echo $k->id_jadwal;?>" title="Input Pelajaran" class="btn btn-custom" style="margin-top:10px;">
							<i class="fa fa-plus"></i> Input Pelajaran
						</button>
						
						<button data-toggle="modal" 
						data-target="#post_kegiatan<?php echo $k->id_jadwal;?>" title="Input Pelajaran" class="btn btn-custom" style="margin-top:10px;">
							<i class="fa fa-plus"></i> Input Kegiatan
						</button>
					</div></div>					
					<?php
					include "modal_post_kegiatan.php";
					include "modal_post_pelajaran.php";
					?>
					
					</div>
				</div>
				</div>								
				
				<?php
				include "modal_delete_hari.php";
			}
			?>
			</div><!-- /.Hari Accordion -->
		</div>
		
		<!-- POST HARI -->
		<div class="row"><div class="col-md-12">
		<button data-toggle="modal" 
		data-target="#post_hari" title="Input Hari" class="btn btn-custom">
			<i class="fa fa-plus"></i> Input Hari
		</button>
		</div></div>
		
		<?php
		include "modal_post_hari.php";
	}
	?>
	</div>

</div>
</div><!-- /.content -->

	<?php 	

}
?>

</div><!-- /.row -->
