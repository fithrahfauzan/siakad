<div class="modal fade" id="post_hari" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<?php 
			$hidden = array('id_kelas'=>$id_kelas);
			echo form_open('akademik/jadwal/','',$hidden);
			?>
			
			<!-- Header -->
			<div class="modal-header btn-custom">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Input Hari - Kelas <?php echo $kelas;?><br>
					<?php echo $cur_tahun;?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				<div class="form-group">
					<label>Hari <span class="asterik">*</span> :</label>
					<?php echo hari_box('hari','','class="form-control" required');?>		
				</div>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php echo form_submit('post_hari','Simpan','class="btn btn-custom"');?>	
				</form>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		
	</div>
</div>