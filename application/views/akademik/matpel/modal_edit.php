<div class="modal fade" id="edit<?php echo $r->id_matpel;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<?php 
			$hidden = array('id'=>$r->id_matpel);
			echo form_open('akademik/matpel/','',$hidden); 
			?>
			
			<!-- Header -->
			<div class="modal-header btn-info">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Edit Mata Pelajaran ID #<?php echo $r->id_matpel;?></h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				<div class="form-group">
					<label>Nama Agama <span class="asterik">*</span> :</label>
					<?php echo form_input('matpel', $r->matpel, 'class="form-control" maxlength="50" required placeholder="Nama Mata Pelajaran ..."');?>	
				</div>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php echo form_submit('edit','Simpan','class="btn btn-info"');?>	
				</form>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		
	</div>
</div>