<div class="modal fade" id="aktif<?php echo $r->id_tahun_ajar;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<?php
			$hidden = array('id'=>$r->id_tahun_ajar,'tahun_ajaran'=>$r->tahun_ajar);
			echo form_open('akademik/tahun_ajar/','',$hidden);
			?>
			
			<!-- Header -->
			<div class="modal-header btn-primary">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Aktifkan (<?php echo $r->tahun_ajar;?>)</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				<div class="heading-inline">Semester :</div>
				<?php
				echo form_dropdown('semester',array('Ganjil'=>'Ganjil','Genap'=>'Genap'),'Ganjil', 'class="form-control form-control-custom"');
				?>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php echo form_submit('aktif','Aktifkan','class="btn btn-primary"'); ?>				
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
			
			</form>
		</div>
		
	</div>
</div>