<div class="modal fade" id="post" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<?php echo form_open('akademik/tahun_ajar/'); ?>
			
			<!-- Header -->
			<div class="modal-header btn-custom">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Input Tahun Ajaran Baru</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				<div class="form-group">
					<label>Tahun Ajaran <span class="asterik">*</span> :</label>
					<?php echo tahun_ajar();?>	
				</div>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php echo form_submit('post','Simpan','class="btn btn-custom"');?>					
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
			
			</form>
		</div>
		
	</div>
</div>