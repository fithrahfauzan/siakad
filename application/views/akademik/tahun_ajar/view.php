<div>
<button data-toggle="modal" data-target="#post" class="btn btn-custom btn-no-radius-bottom">
	<i class="fa fa-plus"></i> Input Data
</button>
</div>

<div class="panel panel-default panel-no-radius-top col-md-9">
<div class="panel-body">
<?php
//message
if($this->session->flashdata('success') != ""){
	echo alert_c("save_success");
}
if($this->session->flashdata('delete') != ""){
	echo alert_c("delete_success");
}
if($this->session->flashdata('duplicate') != ""){
	echo alert_c("duplicate");
}
if($this->session->flashdata('aktif') != ""){
	echo '
		<div class="alert alert-success fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Tahun ajaran '.$this->session->flashdata("aktif").' diaktifkan.
		</div>
	';
}
if($this->session->flashdata('update') != ""){
	echo '
		<div class="alert alert-success fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Tahun ajaran '.$this->session->flashdata("update").' berhasil diupdate.
		</div>
	';
}
?>
	<table id="datatables" class="table table-striped table-hover style-table">
	<thead>
		<tr>
			<th>ID</th>
			<th class="defaultSort sortDesc">Tahun Ajaran</th>
			<th class="t-ket">Aktif</th>
			<th class="t-ket">Semester Aktif</th>
			<th class="t-action"></th>
		</tr>
	</thead>
	<tbody>
	<?php 
	foreach ($record as $r)
	{
	?>
		<tr>
			<td align="center"><?php echo $r->id_tahun_ajar; ?></td>
			<td align="center"><?php echo $r->tahun_ajar; ?></td>
			<td align="center">
				<?php 
				if($r->aktif=='Y'){
					echo "<i class='fa fa-check'></i>";
				}
				else{
					echo "<i class='fa fa-times'></i>";
				}
				?>
			</td>
			<td align="center">
				<?php
					if($r->semester_aktif == "")
						echo "-";
					else{
						echo $r->semester_aktif; 
						?>
						<!-- edit -->
						<button data-toggle="modal" data-target="#edit<?php echo $r->id_tahun_ajar;?>"
						rel="tooltip" data-trigger="hover" title="Edit" 
						class="btn btn-xs btn-info">
							<i class="fa fa-pencil"></i>
						</button>
						<?php
					}
				?>
				
			</td>
			<td align="center">
				<!-- aktif -->
				<button data-toggle="modal" data-target="#aktif<?php echo $r->id_tahun_ajar;?>"
				rel="tooltip" data-trigger="hover" title="Aktifkan" 
				class="btn btn-xs btn-primary" <?php if($r->aktif=='Y')echo 'disabled';?>>
					<i class='fa fa-check'></i>
				</button>
				
				<!-- delete -->
				<button data-toggle="modal" data-target="#delete<?php echo $r->id_tahun_ajar;?>"
				rel="tooltip" data-trigger="hover" title="Delete" 
				class="btn btn-xs btn-danger" <?php if($r->aktif=='Y')echo 'disabled';?>>
					<i class="fa fa-times"></i>
				</button>
			</td>
		</tr>
		
		<?php 
		include "modal_edit.php";
		include "modal_aktifkan.php";
		include "modal_delete.php";
		
	}
	?>
	</tbody>
</table>
</div>
</div>

<?php include "modal_post.php";?>