<?php $this->load->view('theme_header'); ?>

<div id="page-content-wrapper">
	<div class="container-fluid padding0">
		<div class="row">
		
			<div class="col-md-12">
				
				<!-- Title Box -->
				<h1 class="head-title">
					<?php 
						echo"
							<i class='".menu_info('icon')."'></i>
							$title ::: $subtitle
						";
					?>
					
					<!-- Refresh -->
					<div class="pull-right">
						<button onclick="window.location.reload();" rel="tooltip" 
						data-trigger="hover" title="Refresh" 
						class="btn btn-xs btn-default">
							<i class="fa fa-refresh"></i>
						</button>
					</div>
				</h1>
				
				<!-- Content -->
				<div class="main">
					<?php echo $contents;?>
				</div>
			</div>
			
		</div>
	</div>
</div><!-- /#page-content-wrapper -->

<?php $this->load->view('theme_footer'); ?>