<div class="modal fade" id="edit<?php echo $r->id_agama;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<?php 
			$hidden = array('id'=>$r->id_agama);
			echo form_open('config/app_agama/','',$hidden); 
			?>
			
			<!-- Header -->
			<div class="modal-header btn-info">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Edit Data ID #<?php echo $r->id_agama;?></h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				<div class="form-group">
					<label>Nama Agama <span class="asterik">*</span> :</label>
					<?php echo form_input('agama', $r->agama, 'class="form-control" maxlength="50" required placeholder="Nama Agama ..."');?>		
				</div>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php echo form_submit('edit','Simpan','class="btn btn-info"');?>					
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
			
			</form>
		</div>
		
	</div>
</div>