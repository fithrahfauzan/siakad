<div class="modal fade" id="delete<?php echo $r->id_agama;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
		
			<!-- Header -->
			<div class="modal-header btn-danger">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Delete Data ID #<?php echo $r->id_agama;?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				Anda yakin ingin menghapus?
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php
				$hidden = array('id'=>$r->id_agama);
				echo form_open('config/app_agama/','',$hidden);
					echo form_submit('delete','Delete','class="btn btn-danger"');
				?>	
				</form>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		
	</div>
</div>