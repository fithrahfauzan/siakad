<div class="modal fade" id="edit<?php echo $r->id_datakelas;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<?php 
			$hidden = array('id'=>$r->id_datakelas);
			echo form_open('config/app_kelas/','',$hidden); 
			?>
			
			<!-- Header -->
			<div class="modal-header btn-info">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Edit Kelas ID #<?php echo $r->id_datakelas;?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				<div class="form-group">
					<label>Tingkat Kelas <span class="asterik">*</span> :</label>
					<?php echo form_dropdown('tingkat_kelas',tingkat_kelas("combobox"),$r->tingkat_kelas, 'class="form-control" required');?>			
				</div>
				<div class="form-group">
					<label>Kelas <span class="asterik">*</span> :</label>
					<?php num_box("kelas","A","J",$r->kelas,"required");?>				
				</div>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php echo form_submit('edit','Simpan','class="btn btn-info"');?>					
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
			
			</form>
		</div>
		
	</div>
</div>