<div class="modal fade" id="delete<?php echo $r->id_datakelas;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
		
			<!-- Header -->
			<div class="modal-header btn-danger">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Delete Kelas ID #<?php echo $r->id_datakelas;?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				Anda yakin ingin menghapus?<br>
				Data yang berkaitan dengan kelas ini akan terhapus.
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php
				$hidden = array('id'=>$r->id_datakelas);
				echo form_open('config/app_kelas/','',$hidden);
					echo form_submit('delete','Delete','class="btn btn-danger"');
				?>	
				</form>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		
	</div>
</div>