<div>
<button data-toggle="modal" data-target="#post" class="btn btn-custom btn-no-radius-bottom">
	<i class="fa fa-plus"></i> Input Data
</button>
</div>

<div class="panel panel-default panel-no-radius-top col-md-8">
<div class="panel-body">
<?php
//message
if($this->session->flashdata('success') != ""){
	echo alert_c("save_success");
}
if($this->session->flashdata('delete') != ""){
	echo alert_c("delete_success");
}
if($this->session->flashdata('duplicate') != ""){
	echo alert_c("duplicate");
}
?>	
	<table id="datatables" class="table table-striped table-hover style-table">
	<thead>
		<tr>
			<th width="10%">ID</th>
			<th class="defaultSort sortAsc">Kelas</th>
			<th class="t-action"></th>
		</tr>
	</thead>
	<tbody>
	<?php 
	foreach ($record as $r)
	{
	?>
		<tr>
			<td align="center"><?php echo $r->id_datakelas; ?></td>
			<td align="center">
				<?php echo "(".$r->tingkat_kelas.") ".tingkat_kelas($r->tingkat_kelas)." ".$r->kelas; ?>
			</td>
			<td align="center">
				<!-- edit -->
				<button data-toggle="modal" data-target="#edit<?php echo $r->id_datakelas;?>"
				rel="tooltip" data-trigger="hover" title="Edit" 
				class="btn btn-xs btn-info">
					<i class="fa fa-pencil"></i>
				</button>
				
				<!-- delete -->
				<button data-toggle="modal" data-target="#delete<?php echo $r->id_datakelas;?>"
				rel="tooltip" data-trigger="hover" title="Delete" 
				class="btn btn-xs btn-danger">
					<i class="fa fa-times"></i>
				</button>
			</td>
		</tr>
			
		<?php 
		include "modal_edit.php";
		include "modal_delete.php";
		
	}
	?>
	</tbody>
	</table>
	
</div>
</div>

<?php include "modal_post.php";?>