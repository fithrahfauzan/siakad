<div class="row"><div class="col-md-12">
<div class="panel panel-custom">
	<div class="panel-heading">
		Jadwal Pelajaran <?php if(user_info('cur_kelas') == "")echo"";else echo "- Kelas ". user_info('cur_kelas');?>
	</div>
	<div class="panel-body">
	<?php
	if($hari->num_rows()>0){
	?>
		<div class="panel with-nav-tabs panel-customs">
		<div class="panel-heading">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs">
			<?php
			foreach($hari->result() as $h){
				?>
				<li <?php if($h->hari == "1")echo'class="active"';?>>
					<a href="#hari<?php echo $h->hari;?>" data-toggle="tab" aria-expanded="true">
						<?php echo tampil_hari($h->hari);?>
					</a>
				</li>
				<?php
			}
			?>
			</ul>
		</div>
		<div class="panel-body">
			<!-- Tab panes -->
			<div class="tab-content">
			<?php
			foreach($hari->result() as $h){
				?>
				<div class="tab-pane fade <?php if($h->hari == "1")echo"active in";?>" 
				id="hari<?php echo $h->hari;?>">
					<table class="table style-table">
						<thead>
							<tr>
								<th width="5%" class="t-action"></th>
								<th width="15%">Waktu</th>
								<th width="40%">Mata Pelajaran</th>
								<th width="40%">Pengajar</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($jadwal[$h->id_jadwal]->num_rows()>=1){
							$i = 0;
							foreach($jadwal[$h->id_jadwal]->result() as $r){
								// kegiatan lain
								if($r->kegiatan != ""){
									?>
									<tr class="highlight">
										<td></td>
										<td align="center"><?php echo $r->waktu;?></td>
										<td align="center" colspan="2" class="text-uppercase">
											<strong><?php echo $r->kegiatan;?></strong>
										</td>
									</tr>
								<?php
								}
								// kbm
								else{
									$i++;
									?>
									<tr>
										<td align="center"><?php echo $i;?></td>
										<td align="center"><?php echo $r->waktu;?></td>
										<td align="center"><?php echo $r->matpel;?></td>
										<td align="center"><?php echo $r->pengajar;?></td>
									</tr>
								<?php
								}
							}
						}
						// belum  ada data
						else{
							?>
							<tr><td colspan="5" align="center">No Data</td></tr>
							<?php
						}
						?>
						</tbody>
					</table>
				</div>
				<?php
			}
			?>
			</div>
		</div>
		</div>
	<?php
	}
	else{
		?>
		<p class="text-center"><strong>Belum ada Jadwal</strong></p>
	<?php
	}
	?>
	</div>
</div>
<div></div>