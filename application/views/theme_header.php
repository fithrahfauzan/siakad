<?php 
if($this->session->userdata('id_user')==""){
	redirect('');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
	<link rel="icon" href="<?php echo base_url() ?>../../favicon.ico">
	
	<title><?php echo $title;?> - SIAKAD <?php echo setting_info('nama_sekolah');?></title>

	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- MetisMenu CSS -->
    <link href="<?php echo base_url() ?>assets/dist/metisMenu/metisMenu.min.css" rel="stylesheet">
	
	<!-- DatePicker CSS -->
	<link href="<?php echo base_url() ?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />
	
	<!-- DataTables CSS -->
	<link href="<?php echo base_url() ?>assets/dist/dataTables/dataTables.bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/dist/dataTables/dataTables.checkboxes.css" rel="stylesheet">
	
	<!-- Custom styles -->
	<link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	
	<!-- Icon -->
	<link href="<?php echo base_url() ?>assets/dist/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/dist/glyphicons-pro/glyphicons-pro.css" rel="stylesheet">
	
</head>

<body>
  
	<div id="wrapper">
	
		<!-- Sidebar -->
		<div id="sidebar-wrapper">
			<div class="sidebar-content">
				<div class="sidebar-profile">
					<div class="profile-img">
						<img src="<?php echo base_url().'assets/img/foto/'.user_info('foto');?>"/>
					</div>
					<div class="profile-info">
						<h4><?php echo user_info('nama');?></h4>
						<span><?php echo user_info('ket');?></span>
					</div>
				</div>
				<div class="sidebar">
					<ul class="nav" id="side-menu">
						<?php sidebar_nav();?>
						<li>
							<a href="<?php echo base_url() ?>/auth/logout">
								<i class="fa fa-sign-out fa-c"></i> Logout
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div><!-- /#sidebar-wrapper -->
		
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container-fluid padding0">
				<div class="navbar-header">
				  <a href="#menu-toggle" class="side-toggle" id="menu-toggle">
					<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
					<span class="toggle-text">Menu</span>
				  </a>
				</div>
				<div class="info-header">					
					SIAKAD <?php echo setting_info('nama_sekolah');?> - 
					<?php echo $this->m_crud->getW('akademik_tahun_ajar',array('aktif'=>'Y'))->row('tahun_ajar');?>
					
					<span style="font-size:15px">
					Semester <?php echo $this->m_crud->getW('akademik_tahun_ajar',array('aktif'=>'Y'))->row('semester_aktif');?>
					</span>
				</div>
			</div>
		</nav>