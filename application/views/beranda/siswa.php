<!-- welcome -->
<div class="row"><div class="col-md-12">
<div class="panel panel-custom">
	<div class="panel-heading">
		Selamat Datang di Sistem Informasi Akademik <?php echo setting_info('nama_sekolah');?>
	</div>
	<div class="panel-body">
		<p style="font-size:17px;"><?php echo setting_info('welcome_msg');?></p>
	</div>
</div>
</div></div>

<div class="row">

<!-- left -->
<div class="col-md-8">	
	<!-- user profile -->
	<div class="row"><div class="col-md-12">
	<div class="panel panel-custom">
		<div class="panel-heading">
			Data Pribadi
		</div>
		<div class="panel-body">
		<?php
		// Message
		if($this->session->flashdata('error') != ""){
			echo alert_c("foto_error");
		}
		if($this->session->flashdata('success') != ""){
			echo alert_c("save_success");
		}		
		?>
			<div class="row">
				<!-- Foto -->
				<div class="m-foto col-md-4">
				<?php
				$hidden = array('id'=>$r->id_siswa);
				echo form_open_multipart('beranda/siswa','',$hidden);
				?>
					<img src="<?php 
						if($r->foto != ""){
							echo base_url().'assets/img/foto/siswa/'.$r->foto;
						}
						else{
							echo base_url().'assets/img/foto/noprofile.gif';
						}
					?>" width="100%"/>
					<?php echo form_upload('foto','','class="form-control" accept="image/*" required');?>
					<div class="well well-sm well-no-radius all-no-margin-bot">
						<table class="table table-no-margin-bot">
							<tr>
								<td>Format</td>
								<td>jpeg/jpg</td>
							</tr>
							<tr>
								<td>Max Size</td>
								<td>3 mb</td>
							</tr>
						</table>
					</div>
					<?php echo form_submit('edit_foto','Ganti Foto','class="btn btn-default btn-no-radius-top pull-right"');?>
				<?php echo form_close();?>
				</div>
				
				<!-- Data -->
				<div class="col-md-8">
					<table class="table table-list-1 table-no-margin-bot">
					<tbody>
						<!-- NIS -->
						<tr>
							<th width="40%">NIS</th>
							<td>
								<?php echo $r->nis;?>
							</td>
						</tr>
						<!-- Nama -->
						<tr>
							<th>Nama Lengkap</th>
							<td>
								<?php echo $r->nama;?>
							</td>					
						</tr>
						<!-- Jns_kelamin -->
						<tr>
							<th>Jenis Kelamin</th>
							<td>
								<?php echo jns_kelamin($r->jns_kelamin);?>
							</td>
						</tr>
						<!-- TTL -->
						<tr>
							<th>TTL</th>
							<td>
								<?php echo $r->tempat_lahir;?>, <?php echo $r->tanggal_lahir;?>
							</td>
						</tr>
						<!-- Agama -->
						<tr>
							<th>Agama</th>
							<td>
								<?php echo $r->agama;?>
							</td>					
						</tr>
						<!-- Gol Darah -->
						<tr>
							<th>Gol Darah</th>
							<td>
								<?php echo $r->gol_darah;?>
							</td>					
						</tr>
						<!-- Alamat -->
						<tr>
							<th>Alamat</th>
							<td>
								<?php echo alamatfull($r->alamat);?>
							</td>					
						</tr>				
						<!-- Telepon -->
						<tr>
							<th>Telepon</th>
							<td>
								<?php if($r->telp==""){echo"-";} echo $r->telp;?>
							</td>
						</tr>
						
						<!-- Akademik -->
						<tr>
							<th colspan="2">Data Akademik</th>
						</tr>
						<!-- Kelas -->
						<tr>
							<th>Kelas</th>
							<td>
								<?php if(user_info('cur_kelas') == "")echo"-";else echo "Kelas ". user_info('cur_kelas');?>
							</td>
						</tr>
					</tbody>
					</table>
					<!-- edit profile -->
					<button data-toggle="modal" data-target="#editprofile"					
					class="btn btn-default btn-no-radius-top pull-right">
						<i class="fa fa-pencil"></i> Edit Data
					</button>
					<?php include "siswa_modal_edit.php";?>
				</div>
			</div>
		</div>
	</div>
	</div></div>
</div><!-- /.left -->

<!-- right -->
<div class="col-md-4">	
	<!-- user info -->
	<div class="row"><div class="col-md-12">
	<div class="panel panel-custom">
		<div class="panel-heading">
			User Info
		</div>
		<div class="panel-body">
		<?php
		// Message
		if($this->session->flashdata('error_old_pass') != ""){
			echo '
				<div class="alert alert-danger fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Salah Memasukan Password Lama.
				</div>
			';
		}
		if($this->session->flashdata('error_pass_check') != ""){
			echo '
				<div class="alert alert-danger fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Password baru yang dimasukan tidak sama.
				</div>
			';
		}
		if($this->session->flashdata('success_pass') != ""){
			echo '
				<div class="alert alert-success fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Berhasil mengganti password
				</div>
			';
		}
		?>
			<table class="table table-list-2 table-no-margin-bot">
			<tbody>
				<!-- group -->
				<tr>
					<th width="35%">Group</th>
					<td><?php echo user_info('level');?></td>
				</tr>
				<!-- tingkat -->
				<tr>
					<th>Siswa Tingkat</th>
					<td><?php echo tingkat_kelas($tingkat_kelas);?> (<?php echo $tingkat_kelas;?>)</td>
				</tr>
				<!-- username -->
				<tr>
					<th>Nis / Username</th>
					<td><?php echo $r->nis;?></td>
				</tr>
				<!-- Pass -->
				<tr>
					<th>Password</th>
					<td>
						***********
						<!-- edit pass -->
						<button data-toggle="modal" data-target="#editpass"
						rel="tooltip" data-trigger="hover" title="Ganti Password" 
						class="btn btn-xs btn-info pull-right">
							<i class="fa fa-pencil"></i>
						</button>
						<?php include "siswa_modal_edit_pass.php";?>
					</td>
				</tr>
			</tbody>
			</table>
			<p class="text-info all-no-margin-bot"><small>* Dianjurkan mengganti password default apabila belum pernah menggantinya.</small></p>
		</div>
	</div>
	</div></div>
</div><!-- /.right -->

</div>