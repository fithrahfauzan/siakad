<div class="modal fade" id="editprofile" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<?php 
			$hidden = array('id'=>$r->id_siswa);
			echo form_open('beranda/siswa','',$hidden); 
			?>
			
			<!-- Header -->
			<div class="modal-header btn-custom">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Edit Data - <?php echo $r->nama;?>
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				<table class="table table-list-2">
				<tbody>
					<!-- TTL -->
					<tr>
						<th width="28%" class="v-align-middle">TTL <span class="asterik">*</span></th>
						<td>
							<div class="row">
								<div class="col-md-6">
									<?php echo form_input('tempat_lahir', $r->tempat_lahir, 'class="form-control" maxlength="30" required placeholder="Tempat Lahir ..."' );?>
								</div>
								<div class="col-md-6">
									<div class='input-group'>
										<?php echo form_input('tanggal_lahir', $r->tanggal_lahir, ' class="form-control" required placeholder="Tanggal Lahir ..." id="datetimepicker1"' );?>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
						</td>
					</tr>
					<!-- Agama -->
					<tr>
						<th class="v-align-middle">Agama</th>
						<td>
							<?php echo combobox('agama','app_agama','','agama','id_agama','','',$r->id_agama); ?>
						</td>					
					</tr>
					<!-- Gol Darah -->
					<tr>
						<th class="v-align-middle">Gol Darah</th>
						<td>
							<?php echo form_dropdown('gol_darah',gol_darah(),$r->gol_darah, 'class="form-control"')?>
						</td>					
					</tr>
					<!-- Alamat -->
					<tr>
						<th class="v-align-middle">Alamat</th>
						<td>
						<div class="row">
							<!-- Jalan -->
							<div class="form-group col-md-12">
								<label>Nama Jalan</label>
								<?php echo form_input('jalan', alamat_edit('jalan',$r->alamat), 'class="form-control" maxlength="50" placeholder="Nama Jalan ..."' );?>
							</div>
							
							<!-- RT/RW -->
							<div class="form-group col-md-4">
								<label>RT</label>
								<?php echo form_input('rt', alamat_edit('rt',$r->alamat), 'class="form-control" maxlength="4" placeholder="RT ..."' );?>
							</div>
							
							<!-- RW -->
							<div class="form-group col-md-4">
								<label>RW</label>
								<?php echo form_input('rw', alamat_edit('rw',$r->alamat), 'class="form-control" maxlength="4" placeholder="RW ..."' );?>
							</div>
							
							<!-- Nomor -->
							<div class="form-group col-md-4">
								<label>Nomor</label>
								<?php echo form_input('nomor', alamat_edit('nomor',$r->alamat), 'class="form-control" maxlength="5"  placeholder="No ..."' );?>
							</div>
							
							<!-- Kel -->
							<div class="form-group col-md-6">
								<label>Kelurahan</label>
								<?php echo form_input('kel', alamat_edit('kel',$r->alamat), 'class="form-control" maxlength="30" placeholder="Kelurahan ..." ' );?>
							</div>
							
							<!-- Kec -->
							<div class="form-group col-md-6">
								<label>Kecamatan</label>
								<?php echo form_input('kec', alamat_edit('kec',$r->alamat), 'class="form-control" maxlength="30" placeholder="Kecamatan ..."' );?>
							</div>
							
							<!-- Kab-->
							<div class="form-group col-md-6">
								<label>Kabupaten</label>
								<?php echo form_input('kab', alamat_edit('kab',$r->alamat), 'class="form-control" maxlength="30" placeholder="Kabupaten ..."' );?>
							</div>
							
							<!-- Prov -->
							<div class="form-group col-md-6">
								<label>Provinsi</label>
								<?php echo form_input('prov', alamat_edit('prov',$r->alamat), 'class="form-control" maxlength="30" placeholder="Provinsi ..."' );?>
							</div>
						</div>
						</td>					
					</tr>		
					<!-- Telepon -->
					<tr>
						<th class="v-align-middle">Telepon</th>
						<td>
							<?php echo form_input('telp', $r->telp, 'class="form-control" maxlength="28" placeholder="Telepon ..."' );?>
						</td>
					</tr>
				</tbody>
				</table>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php echo form_submit('edit_profile','Simpan','class="btn btn-custom"');?>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
			
			</form>
		</div>
		
	</div>
</div>