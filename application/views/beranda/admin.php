<!-- welcome -->
<div class="row"><div class="col-md-12">
<div class="panel panel-custom">
	<div class="panel-heading">
		Selamat Datang di Sistem Informasi Akademik <?php echo setting_info('nama_sekolah');?>
	</div>
	<div class="panel-body">
		<p style="font-size:17px;"><?php echo setting_info('welcome_msg');?></p>
	</div>
</div>
</div></div>

<div class="row">

<!-- left -->
<div class="col-md-8">

	<!-- siswa -->
	<?php include "admin_modal_detail.php";?>
	<div class="panel panel-custom">
		<div class="panel-heading">
			Statistik Siswa
		</div>		
		<div class="panel-body">			
			<div class="row">
			
				<!-- total -->
				<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-group fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">Total</div>
								<div><?php echo $siswa->num_rows();?> Siswa</div>
							</div>
						</div>
					</div>
				</div>
				</div>
				
				<!-- laki-laki -->
				<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-male fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">Laki - laki</div>
								<div><?php echo $siswa_l->num_rows();?> Siswa</div>
							</div>
						</div>
					</div>
				</div>
				</div>
				
				<!-- perempuan -->
				<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-female fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">Perempuan</div>
								<div><?php echo $siswa_p->num_rows();?> Siswa</div>
							</div>
						</div>
					</div>
				</div>
				</div>
				
			</div>			
		</div>
		<a href="" data-toggle="modal" data-target="#siswa">
			<div class="panel-footer">
				<span class="pull-left">View Details</span>
				<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
				<div class="clearfix"></div>
			</div>
		</a>
	</div><!-- /.siswa -->
	
	<!-- guru -->
	<div class="panel panel-custom">
		<div class="panel-heading">
			Statistik Guru / Pengajar
		</div>
		<div class="panel-body">
				<div class="row">
			
				<!-- total -->
				<div class="col-md-4">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-group fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">Total</div>
								<div><?php echo $guru->num_rows();?></div>
							</div>
						</div>
					</div>
				</div>
				</div>
				
				<!-- laki-laki -->
				<div class="col-md-4">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-male fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">Laki - laki</div>
								<div><?php echo $guru_l->num_rows();?></div>
							</div>
						</div>
					</div>
				</div>
				</div>
				
				<!-- perempuan -->
				<div class="col-md-4">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-female fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">Perempuan</div>
								<div><?php echo $guru_p->num_rows();?></div>
							</div>
						</div>
					</div>
				</div>
				</div>
				
			</div>
		</div>
		<a href="" data-toggle="modal" data-target="#guru">
			<div class="panel-footer">
				<span class="pull-left">View Details</span>
				<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
				<div class="clearfix"></div>
			</div>
		</a>
	</div><!-- /.guru -->

</div><!-- /.left -->

<!-- right -->
<div class="col-md-4">	
	<!-- user info -->
	<?php include "admin_modal_edit_pass.php";?>
	<div class="row"><div class="col-md-12">
	<div class="panel panel-custom">
		<div class="panel-heading">
			User Info
		</div>
		<div class="panel-body">
		<?php
		// Message
		if($this->session->flashdata('error_old_pass') != ""){
			echo '
				<div class="alert alert-danger fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Salah Memasukan Password Lama.
				</div>
			';
		}
		if($this->session->flashdata('error_pass_check') != ""){
			echo '
				<div class="alert alert-danger fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Password baru yang dimasukan tidak sama.
				</div>
			';
		}
		if($this->session->flashdata('success_pass') != ""){
			echo '
				<div class="alert alert-success fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Berhasil mengganti password
				</div>
			';
		}
		?>
			<table class="table table-list-2 table-no-margin-bot">
			<tbody>
				<!-- nama -->
				<tr>
					<th width="35%">Display Name</th>
					<td><?php echo user_info('nama');?></td>
				</tr>
				<!-- group -->
				<tr>
					<th width="35%">Group</th>
					<td><?php echo user_info('ket');?></td>
				</tr>
				<!-- Pass -->
				<tr>
					<th>Password</th>
					<td>
						***********
						<!-- edit pass -->
						<button data-toggle="modal" data-target="#editpass"
						rel="tooltip" data-trigger="hover" title="Ganti Password" 
						class="btn btn-xs btn-info pull-right">
							<i class="fa fa-pencil"></i>
						</button>
						<?php //include "guru_modal_edit_pass.php";?>
					</td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>
	</div></div>
</div><!-- /.right -->

</div>