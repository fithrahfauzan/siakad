<div class="modal fade" id="editpass" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<?php 
			$hidden = array('id'=>$id_user,'tables'=>'app_admin','pk'=>'id_admin');
			echo form_open('beranda','',$hidden);  
			?>
			
			<!-- Header -->
			<div class="modal-header btn-info">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Mengganti Password
				</h4>
			</div>
			<!-- Content -->
			<div class="modal-body">
				<div class="form-group">
					<label>Password Lama <span class="asterik">*</span> :</label>
					<?php echo form_password('old_pass', '', 'class="form-control" maxlength="50" required');?>			
				</div>
				<div class="form-group">
					<label>Password <span class="asterik">*</span> :</label>
					<?php echo form_password('pass', '', 'class="form-control" maxlength="50" required');?>			
				</div>
				<div class="form-group">
					<label>Input Password lagi <span class="asterik">*</span> :</label>
					<?php echo form_password('pass_check', '', 'class="form-control" maxlength="50" required');?>			
				</div>
			</div>
			<!-- Footer -->
			<div class="modal-footer">
				<?php echo form_submit('edit_pass','Simpan','class="btn btn-info"');?>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
			
			</form>
		</div>
		
	</div>
</div>