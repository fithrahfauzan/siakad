<div class="modal fade" id="siswa" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<!-- Header -->
		<div class="modal-header btn-custom">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Detail Siswa</h4>
		</div>
		<!-- Content -->
		<div class="modal-body">
			<table id="datatables" class="table table-striped table-hover style-table" width="100%">
			<thead>
				<tr>
					<th>Nis</th>
					<th class="defaultSort sortAsc">Nama</th>
					<th>Jenis Kelamin</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			foreach ($siswa->result() as $s)
			{
			?>
				<tr>
					<td><?php if($s->nis != ""){echo $s->nis;} else{echo "-";} ?></td>
					<td align="center"><?php echo $s->nama;?></td>
					<td align="center"><?php echo jns_kelamin($s->jns_kelamin);?></td>
				</tr>
			<?php
			}
			?>
			</tbody>
			</table>
		</div>
		<!-- Footer -->
		<div class="modal-footer">				
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		</div>
	</div>		
</div>
</div>

<div class="modal fade" id="guru" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<!-- Header -->
		<div class="modal-header btn-custom">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Detail Guru</h4>
		</div>
		<!-- Content -->
		<div class="modal-body">
			<table id="datatables" class="table table-striped table-hover style-table" width="100%">
			<thead>
				<tr>
					<th class="defaultSort sortAsc">Kode Guru</th>					
					<th>Nama</th>
					<th>Jenis Kelamin</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			foreach ($guru->result() as $g)
			{
			?>
				<tr>
					<td align="center"><?php echo $g->kode_guru;?></td>
					<td align="center"><?php echo $g->nama;?></td>
					<td align="center"><?php echo jns_kelamin($g->jns_kelamin);?></td>
				</tr>
			<?php
			}
			?>
			</tbody>
			</table>
		</div>
		<!-- Footer -->
		<div class="modal-footer">				
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		</div>
	</div>		
</div>
</div>