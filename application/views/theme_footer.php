</div>
<!-- /#wrapper -->

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url() ?>assets/js/jquery-1.12.3.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/js/jquery-1.12.3.min.js"><\/script>')</script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url() ?>assets/js/ie10-viewport-bug-workaround.js"></script>
	
	<!-- Other -->
    <script src="<?php echo base_url() ?>assets/dist/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/sb-admin-2.js"></script>
	
	<!-- DateTimePicker JavaScript -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/moment.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-datetimepicker.min.js"></script>
	<script>
		$(function () {
			$('#datetimepicker1').datetimepicker({
				'format': 'YYYY-MM-DD'
			});
		});
	</script>
	
	<!-- Data Tables JavaScript -->
	<script src="<?php echo base_url() ?>assets/dist/dataTables/dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/dist/dataTables/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/dist/dataTables/dataTables.checkboxes.min.js"></script>
	<script>
	$(document).ready(function(){
		if( $('th.defaultSort').is('.sortAsc') ){
			var sortby = 'asc';
		}
		else if( $('th.defaultSort').is('.sortDesc') ){
			var sortby = 'desc';
		}
		$('[id^="datatables"]').DataTable({
			order: [[$('th.defaultSort').index(), sortby]],
			aLengthMenu: [[10, 25, 50, 75, -1], [10, 25, 50, 75, "All"]],
			pageLength: 10,
			columnDefs: [{
				targets: 't-action',
				sortable: false,
				searchable: false
			}]
		});
		
	});
	</script>
	
	<!-- Tooltip -->
	<script>
	$(function () {
		$('[rel="tooltip"]').tooltip()
	})
	</script>
	
	<!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
		$("#menu-toggle").toggleClass("btn-stoggle")
    });
    </script>
	
  </body>
</html>