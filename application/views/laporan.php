<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title;?> - SIAKAD <?php echo setting_info('nama_sekolah');?></title>
<style>
.wrapper{width: 800px;margin:0 auto; border:1px solid #ccc;padding:10px 0;}
.kop{margin:0 auto;border-bottom: 4px solid #000;border-bottom-style: double; padding-bottom:10px;}
.kop h1, .kop h2, .kop h3, .kop p{margin:0;margin-bottom:5px;}
.kop img{margin: 0px 15px;}
.isi{width:95%;margin:20px auto;}
.highlight{background:#ccc;}
</style>
</head>

<body>
<div class="wrapper">
	<!-- KOP -->
	<table border="0" class="kop" width="95%">
		<tr>
			<td align="center"><img src="<?php echo base_url() ?>assets/img/app/logo.png" alt="Siakad Logo"/></td>
			<td align="center" style="padding-right: 30px;">
				<h1><?php echo setting_info('nama_sekolah');?></h1>
				<h2><?php echo $kop;?></h2>
				<p>Jl. Kemang Raya RT 04/RW 09 Desa Sukmajaya Kota Depok - Jawa Barat, 16412</p>
				<p>Telp. 021-77830720 - Email : kabescikumpa@yahoo.co.id</p>
			</td>
		</tr>
	</table>
	
	<!-- ISI -->
	<div class="isi">
	<?php if($menu == "kelas"){?>
		<p>Kelas : <?php echo $nama_kelas;?></p>
		<p>Wali Kelas : <?php echo $wali_kelas;?></p>
		<table border="1" width="100%" cellspacing="0" cellpadding="2" >
			<thead>
				<tr>
					<th>No</th>
					<th>Nis</th>
					<th>Nama</th>
					<th>Jenis Kelamin</th>
				</tr>
			</thead>
			<tbody>
			<?php
			if($datakelas->num_rows()>0){
				$i = 1;
				foreach($datakelas->result() as $r){
				?>
					<tr>
						<td align="center"><?php echo $i;?></td>
						<td align="center"><?php echo $r->nis;?></td>
						<td><?php echo $r->nama;?></td>
						<td align="center"><?php echo jns_kelamin($r->jns_kelamin);?></td>
					</tr>
					<?php
					$i++;
				}
			}
			// belum ada siswa
			else{
				?>
				<tr><td colspan="5" align="center">Belum ada data</td></tr>
				<?php
			}
			?>
			</tbody>
		</table>
		<p align="right">Jumlah : <?php echo $jumlah;?> Siswa<br>
		Laki - laki : <?php if($jumlahL=="")echo "0"; else echo $jumlahL; ?> Siswa<br>
		Perempuan : <?php  if($jumlahP=="")echo "0"; else echo $jumlahP; ?> Siswa</p>
	<?php 
	}	
	else if($menu == "jadwal"){	
		foreach($hari as $k){
		?>
		<table border="1" width="100%" cellspacing="0" style="margin-bottom:17px;">
		<tr>
			<td style="vertical-align:center" align="center" width="10%">
				<?php echo tampil_hari($k->hari);?>
			</td>
			<td>
				<table border="1" width="100%" cellspacing="0">
				<tr>
					<th width="10%">Jam Ke</th>
					<th width="16%">Waktu</th>
					<th width="37%">Pelajaran</th>								
					<th width="37%">Pengajar</th>					
				</tr>
				<?php
				if($jadwal[$k->id_jadwal]->num_rows()>=1){
					$i = 0;
					foreach($jadwal[$k->id_jadwal]->result() as $r){
						// kegiatan lain
						if($r->kegiatan != ""){
							?>
							<tr class="highlight">
								<td></td>
								<td align="center"><?php echo $r->waktu;?></td>
								<td align="center" colspan="2" class="text-uppercase">
									<strong><?php echo $r->kegiatan;?></strong>
								</td>
							</tr>
							<?php
						}
						//kbm
						else{
							$i++;
							?>
							<tr>
								<td align="center"><?php echo $i;?></td>
								<td align="center"><?php echo $r->waktu;?></td>
								<td align="center"><?php echo $r->matpel;?></td>
								<td align="center"><?php echo $r->pengajar;?></td>
							</tr>
							<?php
						}
					}
				}
				else{
					?>
					<tr><td colspan="5" align="center">Belum ada data</td></tr>
					<?php
				}
				?>
				</table>
			</td>
		</tr>
		</table>
		<?php
		}
	}
	?>
	</div>
</div>



</body>
</html>