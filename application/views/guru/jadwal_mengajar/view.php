<div class="row"><div class="col-md-12">
<div class="panel panel-custom">
	<div class="panel-heading">
		Jadwal Mengajar - <?php echo user_info('nama');?>			
	</div>
	<div class="panel-body">
		<table class="table table-striped table-hover style-table">
		<thead>
			<tr>
				<th>Hari</th>
				<th>Jam Pelajaran</th>
				<th>Kelas</th>				
				<th>Mata Pelajaran</th>
			</tr>
		</thead>
		<tbody>
		<?php
		if($record->num_rows()>0){
			foreach ($record->result() as $r){
			?>
			<tr>
				<td align="center"><?php echo tampil_hari($r->hari); ?></td>
				<td align="center"><?php echo $r->waktu; ?></td>
				<td align="center"><?php echo id_datakelas($r->id_datakelas); ?></td>
				<td align="center"><?php echo $r->matpel; ?></td>
			</tr>
			<?php
			}
		}
		else{
			?>
			<tr><td colspan="4">Belum Ada Data</td></tr>
			<?php
		}
		?>
		</tbody>
		</table>
	</div>
</div>
<div></div>