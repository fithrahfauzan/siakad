<?php 
if($this->session->userdata('id_user')!=""){
	redirect('beranda');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
	<link rel="icon" href="<?php echo base_url() ?>../../favicon.ico">
	
    <title>Login Page - SIAKAD <?php echo setting_info('nama_sekolah');?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo base_url() ?>assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() ?>assets/css/login.css" rel="stylesheet">

</head>

<body>
	<div class="container">
		<div class="row vertical-offset-100">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">                                
						<div class="row-fluid user-row">
							<img src="<?php echo base_url() ?>assets/img/app/logo.png" class="img-responsive" alt="Siakad Logo"/>
							<h2 class="title">
								Sistem Informasi Akademik<br>
								<?php echo setting_info('nama_sekolah');?>
							</h2>
						</div>
					</div>
					<div class="panel-body">
						<form method="post" action="<?php echo base_url()."auth/login_act";?>" accept-charset="UTF-8" role="form" class="form-signin">
							<fieldset>
								<label class="panel-login">
									<div class="login_result">
										<?php
										if($this->uri->segment(3)=="error"){
											echo "
											<span class='label label-danger'>Error : Kesalahan Username atau Password</span>
											";
										}
										?>
									</div>
								</label>
								
								<input name="user" class="form-control" placeholder="Username" id="username" type="text" required autofocus>
								<input name="pass" class="form-control" placeholder="Password" id="inputPassword" type="password" required>
								
								<br>
								<input class="btn btn-lg btn-default btn-block" type="submit" id="login" value="Login »">
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- TweenLite -->
	<script src="<?php echo base_url() ?>assets/js/TweenLite.min.js"></script>
	
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url() ?>assets/js/jquery-1.12.3.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/js/jquery.min.js"><\/script>')</script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url() ?>assets/js/ie10-viewport-bug-workaround.js"></script>
	
	<script type="text/javascript">
	/**
	 * parallax.js
	 * @Author original @msurguy (tw) -> http://bootsnipp.com/snippets/featured/parallax-login-form
	 * @Tested on FF && CH
	 * @Reworked by @kaptenn_com (tw)
	 * @package PARALLAX LOGIN.
	 */

	$(document).ready(function() {
		$(document).mousemove(function(event) {
			TweenLite.to($("body"), 
			.5, {
				css: {
					backgroundPosition: "" + parseInt(event.pageX / 8) + "px " + parseInt(event.pageY / '12') + "px, " + parseInt(event.pageX / '15') + "px " + parseInt(event.pageY / '15') + "px, " + parseInt(event.pageX / '30') + "px " + parseInt(event.pageY / '30') + "px",
					"background-position": parseInt(event.pageX / 8) + "px " + parseInt(event.pageY / 12) + "px, " + parseInt(event.pageX / 15) + "px " + parseInt(event.pageY / 15) + "px, " + parseInt(event.pageX / 30) + "px " + parseInt(event.pageY / 30) + "px"
				}
			})
		})
	})
	</script>
	
</body>
</html>
