<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Custom Helper Sistem Informasi Akademik Sekolah
| -------------------------------------------------------------------
| Author : Fithrah Fauzan
| Year : 2016
|
*/

// Data Setting
function setting_info($data){
	$ci=& get_instance();
	$r = $ci->m_crud->getW('app_setting',array('item'=>$data));
	if($r->num_rows()>0){
		$output = $r->row('content');
	}
	else{
		$output = "";
	}
	
	return $output;
}

// Navigasi
function sidebar_nav(){
	$ci=& get_instance();
	$mainmenu = $ci->m_crud->getW('app_mainmenu', array('level' => user_info('level')))->result();
	foreach ($mainmenu as $m)
	{
		// check sub menu
		$submenu = $ci->m_crud->getW('app_submenu', array('id_mainmenu' => $m->id_mainmenu));
		
		// Jika ada submenu
		if($submenu->num_rows()>0)
		{
			// looping submenu
			echo"
			<li ".menu_info($m->link).">
				<a href='javascript:void(0)'> 
					<i class='".$m->icon." fa-c'></i> "
					.$m->nama.
					"<span class='fa arrow'></span>
				</a>
				<ul class='nav nav-second-level'>";
				foreach ($submenu->result() as $s)
				{
					echo"
					<li ".menu_info($s->link).">
						<a href='".base_url()."$m->link/$s->link'>
							<i class='".$s->icon." fa-c'></i> "
							.$s->nama."
						</a>
					</li>
					";
				}
				echo"
				</ul>
			</li>
			";
			// end looping submenu
		}
		// jika tidak ada submenu
		else{
			echo"
			<li ".menu_info($m->link).">
				<a href='".base_url().$m->link."'>
					<i class='".$m->icon." fa-c'></i> " 
					.$m->nama."
				</a>
			</li>	
			";
		}
	}
}

// Navigasi Menu Info
function menu_info($data){
	$ci=& get_instance();
	$current_page = $ci->uri->segment(1);
	$current_child_page = $ci->uri->segment(1).'/'.$ci->uri->segment(2);
	
	$menu = $ci->m_crud->getW('app_mainmenu', array('level' => user_info('level'), 'link' => $current_page));
	$submenu = $ci->m_crud->getW('app_mainmenu', array('level' => user_info('level'), 'link' => $current_child_page));
	
	// ambil icon menu
	if($data == 'icon'){
		if($menu->num_rows()>"0"){
			$output = $menu->row('icon');
		}
		else{
			$output = $submenu->row('icon');
		}
	}
	
	// sidebar current menu
	else if($data == $current_page && $ci->uri->segment(2) == ""){
		$output = "class='current'";
	}
	else if($data == $current_child_page){
		$output = "class='current'";
	}
	else if($data == $current_page){
		$output = "class='active current'";
	}
	else if($current_page.'/'.$data == $current_child_page){
		$output = "class='current'";
	}
	else{
		$output = "";
	}
	
	return $output;
}

// Informasi User
function user_info($data){
	$ci=& get_instance();
	$id_user = $ci->session->userdata('id_user');
	$level_user = $ci->session->userdata('level');
	
	$query_tahun = $ci->m_crud->getW('akademik_tahun_ajar',array('aktif'=>'y'));
	$cur_tahun = $query_tahun->row('id_tahun_ajar');
	$cur_semester = $query_tahun->row('semester_aktif');
	
	// admin
	if($level_user == 'Admin'){
		$r = $ci->m_crud->getByID('app_admin','id_admin',$id_user)->row_array();
		$foto = 'admin.png';
		$nama = $r['nama'];
		$ket = "Administrator";
	}
	// guru
	else if($level_user == 'Guru'){
		$r = $ci->m_crud->getByID('master_guru','id_guru',$id_user)->row_array();
		$cek_foto = $r['foto'];
		if($cek_foto == "")
			$foto = 'noprofile.gif';
		else
			$foto = 'guru/'.$r['foto'];
		$nama = $r['nama'];
		$matpel = $ci->m_crud->getByID('akademik_matpel','id_matpel',$r['pddkn_umum'])->row('matpel');
		$ket = "<strong>".$level_user."</strong><br>".$matpel;
	}
	// siswa
	else if($level_user == 'Siswa'){
		$r = $ci->m_crud->getByID('master_siswa','id_siswa',$id_user)->row_array();
		$cek_foto = $r['foto'];
		if($cek_foto == "")
			$foto = 'noprofile.gif';
		else
			$foto = 'siswa/'.$r['foto'];
		$nama = $r['nama'];
		$qkelas = "
			SELECT app_kelas.id_datakelas, kelas FROM master_kelas_detail
			INNER JOIN master_kelas ON master_kelas.id_kelas = master_kelas_detail.id_kelas
			INNER JOIN app_kelas ON app_kelas.id_datakelas = master_kelas.id_datakelas
			WHERE id_tahun_ajar = $cur_tahun AND id_siswa = $id_user
		";
		$kelas = $ci->m_crud->normal($qkelas);
		if($kelas->num_rows()==0){
			$id_kelas = "";
			$cur_kelas = "";
			$ket = "-";
		}
		else{
			$id_kelas = $kelas->row('id_datakelas');
			$cur_kelas = id_datakelas($id_kelas);
			$ket = "Kelas : ".$cur_kelas;
		}
	}
	
	// ambil data
	if($data == 'foto'){
		$output = $foto;
	}
	else if($data == 'nama'){
		$output = $nama;
	}
	else if($data == 'ket'){
		$output = $ket;
	}
	else if($data == 'level'){
		$output = $level_user;
	}
	else if($data == 'cur_kelas'){
		$output = $cur_kelas;
	}
	else if($data == 'id_kelas'){
		$output = $id_kelas;
	}
	
	return $output;
}

// Combobox Gol Darah
function gol_darah(){
	return array(
		'Not Set'=>'-- Not Set --',
		'O'=>'O',
		'A'=>'A',
		'B'=>'B',
		'AB'=>'AB'
	);
}

// Jenis Kelamin
function jns_kelamin($data){
	if($data == "L"){
		$output = "Laki - laki";
	}
	else if($data == "P"){
		$output = "Perempuan";
	}
	return $output;
}

// Alamat
function alamat($data, $record){
	// jalan/desa|rt|rw|norumah|kel|kec|kab/kota|prov
	$arr = explode('|', $record);
	
	if($data == "jalan"){
		$output = $arr[0];
	}
	else if($data == "rt"){
		$output = $arr[1];
	}
	else if($data == "rw"){
		$output = $arr[2];
	}
	else if($data == "nomor"){
		$output = $arr[3];
	}
	else if($data == "kel"){
		$output = $arr[4];
	}
	else if($data == "kec"){
		$output = $arr[5];
	}
	else if($data == "kab"){
		$output = $arr[6];
	}
	else if($data == "prov"){
		$output = $arr[7];
	}
	
	if($output == ""){
		$output = "-";
	}
	return $output;
}
function alamat_edit($data, $record){
	// jalan/desa|rt|rw|norumah|kel|kec|kab/kota|prov
	$arr = explode('|', $record);
	
	if($data == "jalan"){
		$output = $arr[0];
	}
	else if($data == "rt"){
		$output = $arr[1];
	}
	else if($data == "rw"){
		$output = $arr[2];
	}
	else if($data == "nomor"){
		$output = $arr[3];
	}
	else if($data == "kel"){
		$output = $arr[4];
	}
	else if($data == "kec"){
		$output = $arr[5];
	}
	else if($data == "kab"){
		$output = $arr[6];
	}
	else if($data == "prov"){
		$output = $arr[7];
	}
	return $output;
}
function alamatfull($data){
	$arr = explode('|', $data);
	
	// jalan
	if($arr[0]==""){
		$jl = " - ";
	}else{
		$jl = $arr[0];
	}
	
	// rt
	if($arr[1]==""){
		$rt = $arr[1];
	}else{
		$rt = " RT".$arr[1];
	}
	
	// rw
	if($arr[2]==""){
		$rw = $arr[2];
	}else{
		$rw = " / RW".$arr[2];
	}
	
	// no
	if($arr[3]==""){
		$no = $arr[3];
	}else{
		$no = " No. ".$arr[3];
	}
	
	// kel
	$kel = $arr[4];
	
	// kec
	if($arr[5]==""){
		$kec = $arr[5];
	}else{
		$kec = "<br>Kec. ".$arr[5]." ";
	}
	
	// kab
	$kab = $arr[6]." ";
	
	// prov
	$prov = "<br>".$arr[7];
	
	
	return $jl.$rt.$rw.$no.$kec.$kab.$prov;
}

// Tampil kelas dari id_datakelas
function id_datakelas($data){
	$CI =& get_instance();
	$nama_kelas = $CI->m_crud->getW('app_kelas',array('id_datakelas'=>$data));
	$a = $nama_kelas->row('tingkat_kelas');
	$b = $nama_kelas->row('kelas');
	return tingkat_kelas($a)." ".$b;
}

// Tingkat kelas
function tingkat_kelas($data){
	if($data == "combobox"){
		$output = array(
			''=>'-- Not Set --',
			'1'=>'I',
			'2'=>'II',
			'3'=>'III',
			'4'=>'IV',
			'5'=>'V',
			'6'=>'VI'
		);
	}
	else if($data == "1"){
		$output = "I";
	}
	else if($data == "2"){
		$output = "II";
	}
	else if($data == "3"){
		$output = "III";
	}
	else if($data == "4"){
		$output = "IV";
	}
	else if($data == "5"){
		$output = "V";
	}
	else if($data == "6"){
		$output = "VI";
	}
	else{
		$output = "";
	}
	return $output; 
}

// Tampil hari
function tampil_hari($data){
	if($data == 1){
		$output = "Senin";
	}
	else if($data == 2){
		$output = "Selasa";
	}
	else if($data == 3){
		$output = "Rabu";
	}
	else if($data == 4){
		$output = "Kamis";
	}
	else if($data == 5){
		$output = "Jum'at";
	}
	else if($data == 6){
		$output = "Sabtu";
	}
	else if($data == 7){
		$output = "Minggu";
	}	
	//escape
	else{
		$output = "";
	}
	return $output;
}

// Combobox tahun ajar
function tahun_ajar(){
	echo "<select name='tahun_ajar' class='form-control' required>";
	echo "<option value=''>--- Pilih Data ----</option>";
	for($i=2000;$i<=2099;$i++){
		$j = $i + 1;
		echo "<option value='$i/$j'>$i/$j</option>";
	}
	echo "</select>";
}

// Combobox dari relasi tabel database
function combobox($nama,$table,$class,$field,$pk,$kondisi,$sort,$value){
	$CI =& get_instance();
	// sortir
	if(empty($sort)){
		$sorttemp="";
	}
	else{
		$sorttemp=$sort;
	}
	
	// kondisi
	if($kondisi==null){
		$data=$CI->m_crud->getAllSort($table,$sort)->result();  
	}
	else{
		$data=$CI->m_crud->getWSort($table,$kondisi,$sort)->result();
	}
	echo"<div class='$class'><select name='".$nama."' id='".$nama."' class='form-control' required>";
	
	// values
	if(empty($value)){
		echo "<option value=''>-- Pilih Data --</option>";
		foreach ($data as $r){
			echo" <option value=".$r->$pk.">".$r->$field."</option>";
		}
	}
	else{
		foreach ($data as $r){
			echo"<option value='".$r->$pk."' ";
			echo $r->$pk==$value?"selected='selected'":"";
			echo">".$r->$field."</option>";
		}
	}
	echo"</select></div>";
}

// Combobox pengajar
function combopengajar($nama,$table1,$table2,$class,$value){
	$CI =& get_instance();
	$query = "
		SELECT id_guru, nama, matpel
		FROM $table1
		INNER JOIN $table2 ON $table2.id_matpel = $table1.pddkn_umum
		ORDER BY nama ASC
	";
	$data=$CI->m_crud->normal($query)->result();
	
	echo"<div class='$class'><select name='".$nama."' id='".$nama."' class='form-control' required>";
	
	// values
	if(empty($value)){
		echo "<option value=''>-- Pilih Pengajar --</option>";
		foreach ($data as $r){
			echo"<option value=".$r->id_guru.">".$r->nama." (".$r->matpel.")</option>";
		}
	}
	else{
		foreach ($data as $r){
			echo"<option value='".$r->id_guru."' ";
			echo $r->id_guru==$value?"selected='selected'":"";
			echo">".$r->nama." (".$r->matpel.")</option>";
		}
	}
	echo"</select></div>";
}

// Combobox kelas dari database
function combokelas($nama,$table,$class,$field1,$field2,$pk,$sort,$value){
	$CI =& get_instance();
	$data=$CI->m_crud->getAllSort($table,$sort)->result();
	
	echo"<div class='$class'><select name='".$nama."' id='".$nama."' class='form-control' required>";
	
	// values
	if(empty($value)){
		echo "<option value=''>-- Pilih Kelas --</option>";
		foreach ($data as $r){
			echo"<option value=".$r->$pk.">".tingkat_kelas($r->$field1)." ".$r->$field2."</option>";
		}
	}
	else{
		foreach ($data as $r){
			echo"<option value='".$r->$pk."' ";
			echo $r->$pk==$value?"selected='selected'":"";
			echo">".tingkat_kelas($r->$field1)." ".$r->$field2."</option>";
		}
	}
	echo"</select></div>";
}

// Combobox perulangan nomor
function num_box($nama,$min,$max,$data,$tags){
	echo "<select name='$nama' class='form-control' $tags>";
	echo "<option value=''>--- Pilih Data ----</option>";
	for($i=$min;$i<=$max;$i++){
		if($data==$i){
			echo "<option selected='selected' value='$i'>$i</option>";
		}
		else{
			echo "<option value='$i'>$i</option>";
		}
	}
	echo "</select>";
}

// Combobox hari
function hari_box($nama,$value,$tags){	
	echo form_dropdown(
		$nama,
		array(
			'' => '-- Pilih Data --',
			'1' => 'Senin',
			'2' => 'Selasa',
			'3' => 'Rabu',
			'4' => 'Kamis',
			"5" => "Jum'at",
			'6' => 'Sabtu',
			'7' => 'Minggu'
		),
		$value,
		$tags
	);
}

?>