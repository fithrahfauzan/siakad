<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function alert_c($data){
	$ci=& get_instance();
	
	// success save
	if($data == "save_success"){
		$output = '
			<div class="alert alert-success fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Data berhasil disimpan.
			</div>
		';
	}
	
	// tidak ada data yg dipilih
	else if($data == "warning"){
		$output = '
			<div class="alert alert-danger fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Warning: Tidak ada data yang dipilih.
			</div>
		';
	}
	
	// data duplicate
	else if($data == "duplicate"){
		$output = '
			<div class="alert alert-danger fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Error : Duplicate data atau data tidak berubah.
			</div>
		';
	}
	
	// upload foto error
	else if($data == "foto_error"){
		$output = '
			<div class="alert alert-danger fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Ada kesalahan pada upload foto : '.$ci->session->flashdata('error').'
			</div>
		';
	}
	// save tanpa foto
	else if($data == "save_tanpa_foto"){
		$output = '
			<div class="alert alert-info fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Data berhasil disimpan tanpa upload foto.
			</div>
		';
	}
	
	// hapus success
	else if($data == "delete_success"){
		$output = '
			<div class="alert alert-info fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Data berhasil dihapus.
			</div>
		';
	}
	
	// remove success
	else if($data == "remove_success"){
		$output = '
			<div class="alert alert-info fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Data berhasil diremove.
			</div>
		';
	}
	
	
	// escape
	else{
		$output = "";
	}
	
	return $output;
}
?>