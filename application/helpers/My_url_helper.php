<?php
/**
 * Redirect to previous page if http referer is set. Otherwise to server root.
 */
 
if ( ! function_exists('redirect_back'))
{
    function redirect_back()
    {
		$ci=& get_instance();
		if($ci->session->userdata('id_user')==""){
			redirect('auth/login');
		}
        else if(isset($_SERVER['HTTP_REFERER']))
        {
            header('Location: '.$_SERVER['HTTP_REFERER']);
        }
        else
        {
            redirect('');
        }
        exit;
    }
}
?>