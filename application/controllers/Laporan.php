<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporan extends CI_Controller{
		
	// var db tables
	var $tables_siswa = "master_siswa";
	var $tables_guru = "master_guru";
	var $tables_kelas = "master_kelas";
	var $tables_kelas_sub = "master_kelas_detail";
	var $tables_tahun_ajar = "akademik_tahun_ajar";
	var $tables_jadwal = "akademik_jadwal";
	var $tables_jadwal_sub = "akademik_jadwal_detail";
	var $tables_matpel = "akademik_matpel";
	
	function __construct(){
		parent::__construct();
		if($this->session->userdata('level')==""){
			redirect_back();
		}
	}
	
	function index(){
		$menu = $this->session->flashdata('menu');
		
		// LAPORAN KELAS
		if($menu == "kelas"){
			$id_kelas = $this->session->flashdata('id_kelas');
			$query = "
				SELECT 
					id_datakelas, wali_kelas, nis, nama, jns_kelamin, tahun_ajar, semester_aktif
				FROM $this->tables_kelas
				INNER JOIN $this->tables_kelas_sub
					ON $this->tables_kelas_sub.id_kelas = $this->tables_kelas.id_kelas
				INNER JOIN $this->tables_siswa 
					ON $this->tables_siswa.id_siswa = $this->tables_kelas_sub.id_siswa
				INNER JOIN $this->tables_tahun_ajar
					ON $this->tables_tahun_ajar.id_tahun_ajar = $this->tables_kelas.id_tahun_ajar
				WHERE 
					$this->tables_kelas.id_kelas = '$id_kelas'
				ORDER BY
					nama ASC
			";					
			$data['datakelas'] = $this->m_crud->normal($query);			
			$data['nama_kelas'] = id_datakelas($data['datakelas']->row('id_datakelas'));
			
			// wali kelas
			$wali_kelas = $data['datakelas']->row('wali_kelas');
			$data['wali_kelas'] = $this->m_crud->getW($this->tables_guru,array('id_guru'=>$wali_kelas))->row('nama');
			
			// tahun ajar
			$data['tahun_ajar'] = $data['datakelas']->row('tahun_ajar');
			$data['semester'] = $data['datakelas']->row('semester_aktif');
			
			// get jumlah siswa
			$data['jumlah'] = $data['datakelas']->num_rows();
			$qjumlah = "SELECT SUM(jns_kelamin = 'L') AS jumlahL, SUM(jns_kelamin = 'P') AS jumlahP
			FROM ($query) AS tb
			WHERE jns_kelamin = 'L' OR jns_kelamin = 'P'";
			$djumlah = $this->m_crud->normal($qjumlah);
			$data['jumlahL'] = $djumlah->row('jumlahL');
			$data['jumlahP'] = $djumlah->row('jumlahP');
			
			// view
			$data['menu'] = $menu;
			$data['title'] = "Kelas ".$data['nama_kelas'];
			$data['kop'] = "Data Siswa Kelas ".$data['nama_kelas']."<br>Semester ".$data['semester']." Tahun Pelajaran ".$data['tahun_ajar'];
			$this->load->view('laporan',$data);
		}
		
		// LAPORAN KELAS
		else if($menu == "jadwal"){
			$id_kelas = $this->session->flashdata('id_kelas');			
			$queryk = "
				SELECT tahun_ajar, semester_aktif, id_datakelas 
				FROM $this->tables_kelas
				INNER JOIN $this->tables_tahun_ajar
					ON $this->tables_tahun_ajar.id_tahun_ajar = $this->tables_kelas.id_tahun_ajar
				WHERE id_kelas='$id_kelas'
				";
			$datakelas = $this->m_crud->normal($queryk);
			$data['nama_kelas'] = id_datakelas($datakelas->row('id_datakelas'));			
			$data['tahun_ajar'] = $datakelas->row('tahun_ajar');
			$data['semester'] = $datakelas->row('semester_aktif');
			
			// hari
			$data['hari'] = $this->m_crud->getWSort($this->tables_jadwal,array('id_kelas'=>$id_kelas),'hari asc')->result();			
			// jadwal
			foreach($data['hari'] as $r){
				$query_jadwal[$r->id_jadwal] = "
					SELECT
						id_jadwal_detail, waktu, kegiatan,
						$this->tables_matpel.matpel, $this->tables_jadwal_sub.matpel AS id_matpel, 
						$this->tables_guru.nama AS pengajar, $this->tables_jadwal_sub.pengajar AS id_guru
					FROM $this->tables_jadwal_sub					
					LEFT JOIN $this->tables_matpel
						ON $this->tables_matpel.id_matpel = $this->tables_jadwal_sub.matpel
					LEFT JOIN $this->tables_guru
						ON $this->tables_guru.id_guru = $this->tables_jadwal_sub.pengajar
					WHERE id_jadwal = '$r->id_jadwal'
					ORDER BY waktu ASC
				";
				$data['jadwal'][$r->id_jadwal] = $this->m_crud->normal($query_jadwal[$r->id_jadwal]);
			}
			
			// view
			$data['menu'] = $menu;
			$data['title'] = "Jadwal Pelajaran Kelas ".$data['nama_kelas'];
			$data['kop'] = "Jadwal Pelajaran Kelas ".$data['nama_kelas']."<br>Semester ".$data['semester']." Tahun Pelajaran ".$data['tahun_ajar'];
			$this->load->view('laporan',$data);
		}
		
		// escape
		else{
			echo"<script>window.close()</script>";
		}
	}
	
}
?>