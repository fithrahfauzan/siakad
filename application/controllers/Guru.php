<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class guru extends CI_Controller{
	
	var $folder = "guru";
	var $title = "Ruang Guru";
	
	// var db tables
	var $tables_tahun_ajar = "akademik_tahun_ajar";
	var $tables_jadwal = "akademik_jadwal";
	var $tables_jadwal_sub = "akademik_jadwal_detail";
	var $tables_matpel = "akademik_matpel";
	var $tables_kelas = "master_kelas";
	var $tables_app_kelas = "app_kelas";
	
	function __construct(){
		parent::__construct();
		if($this->session->userdata('level')!="Guru"){
			redirect_back();
		}
	}
	
	/****************************
	Jadwal Mengajar
	*****************************/
	function jadwal_mengajar(){
		// var		
		$data['title'] = $this->title;		
		$subfolder = "jadwal_mengajar";
		$id_user = $this->session->userdata('id_user');
		
		// var tahun dan semester
		$query_tahun = $this->m_crud->getW($this->tables_tahun_ajar,array('aktif'=>'y'));
		$id_tahun_ajar = $query_tahun->row('id_tahun_ajar');
		
		// VIEW
		$query = "
			SELECT id_jadwal_detail, hari, waktu, $this->tables_matpel.matpel, id_datakelas
			FROM $this->tables_jadwal_sub
			INNER JOIN $this->tables_jadwal 
			ON $this->tables_jadwal.id_jadwal = $this->tables_jadwal_sub.id_jadwal
			INNER JOIN $this->tables_matpel
			ON $this->tables_matpel.id_matpel = $this->tables_jadwal_sub.matpel
			INNER JOIN
			
			(SELECT id_kelas, $this->tables_kelas.id_datakelas
			FROM $this->tables_kelas 
			INNER JOIN $this->tables_app_kelas 
			ON $this->tables_app_kelas.id_datakelas = $this->tables_kelas.id_datakelas
			WHERE id_tahun_ajar = $id_tahun_ajar) AS $this->tables_kelas
			
			ON $this->tables_kelas.id_kelas = $this->tables_jadwal .id_kelas			
			WHERE pengajar = $id_user		
			ORDER BY hari ASC, waktu ASC, id_datakelas ASC
		";		
		$data['record'] = $this->m_crud->normal($query);
		
		$data['subtitle'] = "Jadwal Mengajar";
		$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
	}
	
}
	
?>