<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class siswa extends CI_Controller{
	
	var $folder = "siswa";
	var $title = "Ruang Siswa";
	
	// var db tables
	var $tables_guru = "master_guru";
	var $tables_kelas = "master_kelas";
	var $tables_jadwal = "akademik_jadwal";
	var $tables_jadwal_sub = "akademik_jadwal_detail";	
	var $tables_tahun_ajar = "akademik_tahun_ajar";
	var $tables_matpel = "akademik_matpel";
	var $tables_app_kelas = "app_kelas";
	
	function __construct(){
		parent::__construct();
		if($this->session->userdata('level')!="Siswa"){
			redirect_back();
		}
	}
	
	/****************************
	Jadwal Pelajaran
	*****************************/
	function jadwal_pelajaran(){
		// var		
		$data['title'] = $this->title;		
		$subfolder = "jadwal_pelajaran";
		$id_user = $this->session->userdata('id_user');
		
		// var tahun, semster, kelas
		$query_tahun = $this->m_crud->getW($this->tables_tahun_ajar,array('aktif'=>'y'));
		$id_tahun_ajar = $query_tahun->row('id_tahun_ajar');	
		$id_kelas = user_info('id_kelas');
		
		// get hari
		$query_hari = "
			SELECT hari, id_jadwal FROM $this->tables_jadwal
			INNER JOIN $this->tables_kelas ON $this->tables_kelas.id_kelas = $this->tables_jadwal.id_kelas
			WHERE id_tahun_ajar = '$id_tahun_ajar' AND id_datakelas = '$id_kelas'
			ORDER BY hari
		";
		$data['hari'] = $this->m_crud->normal($query_hari);
		
		// get jadwal per kelas
		foreach($data['hari']->result() as $r){
			$query_jadwal[$r->id_jadwal] = "
				SELECT
					id_jadwal_detail, waktu, kegiatan,
					$this->tables_matpel.matpel, $this->tables_jadwal_sub.matpel AS id_matpel, 
					$this->tables_guru.nama AS pengajar, $this->tables_jadwal_sub.pengajar AS id_guru
				FROM $this->tables_jadwal_sub				
				LEFT JOIN $this->tables_matpel
					ON $this->tables_matpel.id_matpel = $this->tables_jadwal_sub.matpel
				LEFT JOIN $this->tables_guru
					ON $this->tables_guru.id_guru = $this->tables_jadwal_sub.pengajar
				WHERE id_jadwal = '$r->id_jadwal'
				ORDER BY waktu ASC
			";
			$data['jadwal'][$r->id_jadwal] = $this->m_crud->normal($query_jadwal[$r->id_jadwal]);
		}
		
		
		$data['subtitle'] = "Jadwal Pelajaran";
		$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
	}
	
}
?>