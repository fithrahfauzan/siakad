<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class welcome extends CI_Controller{
	
	var $title = "Welcome";
	
	function index(){
		$data['title'] = $this->title;
		$data['subtitle'] = "";
		$this->template->load('theme','lorem',$data);
	}
}
?>