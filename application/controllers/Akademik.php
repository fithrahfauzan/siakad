<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class akademik extends CI_Controller{
	
	var $folder = "akademik";
	var $title  = "Akademik";
	
	// var db tables
	var $tables_guru = "master_guru";
	var $tables_kelas = "master_kelas";	
	var $tables_tahun_ajar = "akademik_tahun_ajar";
	var $tables_matpel = "akademik_matpel";
	var $tables_jadwal = "akademik_jadwal";
	var $tables_jadwal_sub = "akademik_jadwal_detail";
	var $tables_nilai = "akademik_nilai";
	var $tables_nilai_sub = "akademik_nilai_detail";
	
	function __construct(){
		parent::__construct();
		if($this->session->userdata('level')!="Admin"){
			redirect_back();
		}
	}
	
	function index(){
		redirect('akademik/tahun_ajar');
	}
	
	/****************************
	Tahun Ajaran
	*****************************/
	function tahun_ajar(){
		// var
		$tables = $this->tables_tahun_ajar;
		$pk = "id_tahun_ajar";
		$subfolder = "tahun_ajar";
		$data['title'] = $this->title;
		
		// POST
		if(isset($_POST['post'])){
			$tahun_ajar = $this->input->post('tahun_ajar');
			$check = $this->m_crud->getW($tables, array('tahun_ajar'=>$tahun_ajar));
			if($check->num_rows()>=1){
				$this->session->set_flashdata('duplicate', '1');
				redirect($this->folder.'/'.$subfolder);
			}
			else{
				$data = array('tahun_ajar' => $tahun_ajar);
				$this->m_crud->insert($tables,$data);
				$this->session->set_flashdata('success', '1');
				redirect($this->folder.'/'.$subfolder);
			}	
		}			
		
		// DELETE
		if(isset($_POST['delete'])){
			$id = $this->input->post('id');
			$this->m_crud->delete($tables,$pk,$id);
			$this->session->set_flashdata('delete', '1');
			redirect($this->folder.'/'.$subfolder);
		}
		
		// AKTIF
		else if(isset($_POST['aktif'])){
			$id = $this->input->post('id');
			$tahun_ajaran = $this->input->post('tahun_ajaran');
			$semester = $this->input->post('semester');
			
			// non-aktif
			$nonaktif = array('aktif' => 'T'); 
			$this->m_crud->update($tables, array('semester_aktif'=>''), 'aktif', 'Y');
			$this->m_crud->update($tables, $nonaktif, 'aktif', 'Y');
			
			// aktif
			$aktif = array('aktif' => 'Y');
			$this->m_crud->update($tables, $aktif, $pk, $id);
			$this->m_crud->update($tables, array('semester_aktif'=>$semester), $pk, $id);
				
			$this->session->set_flashdata('aktif', $tahun_ajaran);
			redirect($this->folder.'/'.$subfolder);
		}	
		
		// EDIT
		else if(isset($_POST['edit'])){
			$id = $this->input->post('id');
			$tahun_ajaran = $this->input->post('tahun_ajaran');
			$semester = $this->input->post('semester');
			
			$this->m_crud->update($tables, array('semester_aktif'=>$semester), $pk, $id);
			$this->session->set_flashdata('update', $tahun_ajaran);
			redirect($this->folder.'/'.$subfolder);
		}
		
		// VIEW
		else{
			$data['subtitle'] = "Tahun Ajaran";
			$data['record'] = $this->m_crud->getAll($tables)->result();
			$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
		}
	}
	
	/****************************
	Mata Pelajaran
	*****************************/
	function matpel(){
		// var
		$tables = $this->tables_matpel;
		$pk = "id_matpel";
		$subfolder = "matpel";
		$data['title'] = $this->title;
		
		// POST
		if(isset($_POST['post'])){
			$data = array('matpel'=> $this->input->post('matpel'));
			$this->m_crud->insert($tables,$data);
			$this->session->set_flashdata('success', '1');
			redirect($this->folder.'/'.$subfolder);
		}
		
		// EDIT
		else if(isset($_POST['edit'])){
			$id = $this->input->post('id');
			$data = array('matpel'=> $this->input->post('matpel'));
			$this->m_crud->update($tables, $data, $pk, $id);
			$this->session->set_flashdata('success', '1');
			redirect_back();
		}
		
		// DELETE
		if(isset($_POST['delete'])){
			$id = $this->input->post('id');
			$this->m_crud->delete($tables,$pk,$id);
			$this->session->set_flashdata('delete', '1');
			redirect($this->folder.'/'.$subfolder);
		}
		
		// VIEW
		else{
			$data['subtitle'] = "Mata Pelajaran";			
			$data['record'] = $this->m_crud->getAll($tables)->result();
			$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
		}
	}
	
	/****************************
	Jadwal Pelajaran
	*****************************/
	function jadwal(){
		// var
		$tables = $this->tables_jadwal;
		$subtables = $this->tables_jadwal_sub;	
		$pk = "id_jadwal";
		$subfolder = "jadwal";
		$data['title'] = $this->title;	
		
		// SET SESSION MENU FILTER
		if(isset($_POST['session_filter'])){
			//generate db master_kelas
			if(isset($_POST['generate'])){
				$data = array(
					'id_kelas'=>'',
					'id_datakelas'=>$this->input->post('kelas'),
					'id_tahun_ajar'=>$this->input->post('tahun_ajar'),
					'wali_kelas'=>''
				);				
				$this->m_crud->insert($this->tables_kelas,$data);
			}
			
			//filtering
			$data = array(
				'filtered' => '1',
				'kelas' => $this->input->post('kelas'),
				'tahun_ajar' => $this->input->post('tahun_ajar')
			);
			$this->session->set_flashdata($data);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// CETAK
		else if(isset($_POST['cetak'])){									
			$data = array(
				'menu'=>'jadwal',
				'id_kelas'=>$this->input->post('id_kelas')
			);
			$this->session->set_flashdata($data);
			redirect('laporan');
		}
		
		// POST HARI
		else if(isset($_POST['post_hari'])){
			$id_kelas = $this->input->post('id_kelas');
			$hari = $this->input->post('hari');
			$check_hari = $this->m_crud->getW($tables,array('hari'=>$hari,'id_kelas'=>$id_kelas));
			
			// duplicate found
			if($check_hari->num_rows()>=1){
				// message
				$cur_menu = $this->m_crud->getW($this->tables_kelas,array('id_kelas'=>$id_kelas));
				$data_filter = array(				
					'duplicate' => '1',
					'filtered' => '1',					
					'kelas' => $cur_menu->row('id_datakelas'),
					'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
				);				
			}
			// duplicate not found
			else{				
				$data = array(
					'id_kelas'=> $id_kelas,
					'hari'=> $hari
				);
				$this->m_crud->insert($tables,$data);
				// message
				$cur_menu = $this->m_crud->getW($this->tables_kelas,array('id_kelas'=>$id_kelas));
				$data_filter = array(				
					'success' => '1',
					'filtered' => '1',					
					'kelas' => $cur_menu->row('id_datakelas'),
					'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
				);				
			}
			
			$this->session->set_flashdata($data_filter);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// DELETE HARI
		else if(isset($_POST['delete_hari'])){			
			$id = $this->input->post('id_jadwal');
			$id_kelas = $this->input->post('id_kelas');
			$this->m_crud->delete($tables,$pk,$id);	
			
			// message
			$cur_menu = $this->m_crud->getW($this->tables_kelas,array('id_kelas'=>$id_kelas));
			$data_filter = array(
				'delete' => '1',
				'filtered' => '1',				
				'kelas' => $cur_menu->row('id_datakelas'),
				'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
			);
			$this->session->set_flashdata($data_filter);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// POST PELAJARAN
		else if(isset($_POST['post_pelajaran'])){
			$id_kelas = $this->input->post('id_kelas');
			$id_jadwal = $this->input->post('id_jadwal');
			$data = array(
				'id_jadwal' => $id_jadwal,			
				'waktu' => $this->input->post('waktu'),
				'matpel' => $this->input->post('matpel'),
				'kegiatan'=> "",
				'pengajar' => $this->input->post('pengajar')
			);
			$this->m_crud->insert($subtables,$data);
			
			// message
			$cur_menu = $this->m_crud->getW($this->tables_kelas,array('id_kelas'=>$id_kelas));
			$data_filter = array(				
				'success' => '1',
				'filtered' => '1',
				'cur_hari' => $id_jadwal,
				'kelas' => $cur_menu->row('id_datakelas'),
				'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
			);
			$this->session->set_flashdata($data_filter);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// EDIT PELAJARAN
		else if(isset($_POST['edit_pelajaran'])){
			$id = $this->input->post('id');
			$id_jadwal = $this->input->post('id_jadwal');
			$id_kelas = $this->input->post('id_kelas');
			$data = array(		
				'waktu' => $this->input->post('waktu'),
				'matpel' => $this->input->post('matpel'),
				'pengajar' => $this->input->post('pengajar')
			);
			$this->m_crud->update($subtables, $data, 'id_jadwal_detail', $id);
			
			// message
			$cur_menu = $this->m_crud->getW($this->tables_kelas,array('id_kelas'=>$id_kelas));
			$data_filter = array(				
				'success' => '1',
				'filtered' => '1',
				'cur_hari' => $id_jadwal,
				'kelas' => $cur_menu->row('id_datakelas'),
				'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
			);
			$this->session->set_flashdata($data_filter);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// POST KEGIATAN
		else if(isset($_POST['post_kegiatan'])){
			$id_kelas = $this->input->post('id_kelas');
			$id_jadwal = $this->input->post('id_jadwal');
			$data = array(
				'id_jadwal' => $id_jadwal,			
				'waktu' => $this->input->post('waktu'),
				'matpel' => '0',
				'kegiatan'=> $this->input->post('kegiatan'),
				'pengajar' => '0'
			);
			$this->m_crud->insert($subtables,$data);
			
			// message
			$cur_menu = $this->m_crud->getW($this->tables_kelas,array('id_kelas'=>$id_kelas));
			$data_filter = array(				
				'success' => '1',
				'filtered' => '1',
				'cur_hari' => $id_jadwal,
				'kelas' => $cur_menu->row('id_datakelas'),
				'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
			);
			$this->session->set_flashdata($data_filter);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// EDIT KEGIATAN
		else if(isset($_POST['edit_kegiatan'])){
			$id = $this->input->post('id');
			$id_jadwal = $this->input->post('id_jadwal');
			$id_kelas = $this->input->post('id_kelas');
			$data = array(		
				'waktu' => $this->input->post('waktu'),
				'kegiatan'=> $this->input->post('kegiatan')
			);
			$this->m_crud->update($subtables, $data, 'id_jadwal_detail', $id);
			
			// message
			$cur_menu = $this->m_crud->getW($this->tables_kelas,array('id_kelas'=>$id_kelas));
			$data_filter = array(								
				'success' => '1',
				'filtered' => '1',
				'cur_hari' => $id_jadwal,
				'kelas' => $cur_menu->row('id_datakelas'),
				'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
			);
			$this->session->set_flashdata($data_filter);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// DELETE JADWAL
		else if(isset($_POST['delete_jadwal'])){
			$id = $this->input->post('id');
			$id_jadwal = $this->input->post('id_jadwal');
			$id_kelas = $this->input->post('id_kelas');
			$this->m_crud->delete($subtables,'id_jadwal_detail',$id);	
			
			// message
			$cur_menu = $this->m_crud->getW($this->tables_kelas,array('id_kelas'=>$id_kelas));
			$data_filter = array(
				'delete' => '1',
				'filtered' => '1',
				'cur_hari' => $id_jadwal,
				'kelas' => $cur_menu->row('id_datakelas'),
				'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
			);
			$this->session->set_flashdata($data_filter);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// VIEW
		else if($this->uri->segment(3)=="filter"){
			//escape
			if($this->session->flashdata('filtered')== ""){
				redirect($this->folder.'/'.$subfolder);
			}			
			else{
				$kelas = $this->session->flashdata('kelas');
				$tahun_ajar = $this->session->flashdata('tahun_ajar');
				$semester = $this->session->flashdata('semester');
				
				$data['checkkelasdb'] = $this->m_crud->getW($this->tables_kelas,array(
						'id_datakelas'=>$kelas,
						'id_tahun_ajar'=>$tahun_ajar
					));					
				$id_kelas = $data['checkkelasdb']->row('id_kelas');
				$data['id_kelas'] = $id_kelas;				
				
				// hari
				$data['hari'] = $this->m_crud->getWSort($tables,array('id_kelas'=>$id_kelas),'hari asc')->result();
				
				// jadwal
				foreach($data['hari'] as $r){
					$query_jadwal[$r->id_jadwal] = "
						SELECT
							id_jadwal_detail, waktu, kegiatan,
							$this->tables_matpel.matpel, $subtables.matpel AS id_matpel, 
							$this->tables_guru.nama AS pengajar, $subtables.pengajar AS id_guru
						FROM $subtables					
						LEFT JOIN $this->tables_matpel
							ON $this->tables_matpel.id_matpel = $subtables.matpel
						LEFT JOIN $this->tables_guru
							ON $this->tables_guru.id_guru = $subtables.pengajar
						WHERE id_jadwal = '$r->id_jadwal'
						ORDER BY waktu ASC
					";
					$data['jadwal'][$r->id_jadwal] = $this->m_crud->normal($query_jadwal[$r->id_jadwal]);
				}
				
				// get nama kelas
				$data['kelas'] = id_datakelas($kelas);				
				
				// get cur menu filter
				$data['cur_idkelas'] = $kelas;
				$data['cur_idtahun'] = $tahun_ajar;
				$data['cur_tahun'] = $this->m_crud->getW($this->tables_tahun_ajar,array('id_tahun_ajar'=>$tahun_ajar))->row('tahun_ajar');						
				
				$data['subtitle'] = "Jadwal Pelajaran";
				$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
			}
		}
			
		// MENU FILTER
		else{			
			$data['subtitle'] = "Jadwal Pelajaran";
			$data['cur_idkelas'] = "";
			$data['cur_idtahun'] = $this->m_crud->getW($this->tables_tahun_ajar,array('aktif'=>'Y'))->row('tahun_ajar');
			
			$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
		}
	}
}