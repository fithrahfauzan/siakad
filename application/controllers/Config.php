<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class config extends CI_Controller{
	
	var $folder = "config";
	var $title  = "Config";
	
	// var db tables
	var $tables_appkelas = "app_kelas";
	var $tables_agama = "app_agama";

	
	function __construct(){
		parent::__construct();
		if($this->session->userdata('level')!="Admin"){
			redirect_back();
		}
	}
	
	function index(){
		redirect('config/app_kelas');
	}
	
	/****************************
	APP Kelas
	*****************************/
	function app_kelas(){
		// var
		$tables = $this->tables_appkelas;
		$pk = "id_datakelas";
		$subfolder = "app_kelas";
		$data['title'] = $this->title;
		
		// POST
		if(isset($_POST['post'])){
			$tingkat_kelas = $this->input->post('tingkat_kelas');
			$kelas = $this->input->post('kelas');
			$check_kelas = $this->m_crud->getW($tables,array('tingkat_kelas'=>$tingkat_kelas,'kelas'=>$kelas));
			// duplicate found
			if($check_kelas->num_rows()>=1){
				$this->session->set_flashdata('duplicate', '1');
				redirect($this->folder.'/'.$subfolder);
			}
			//duplicate not found
			else{
				$data = array(
					'tingkat_kelas'=> $this->input->post('tingkat_kelas'),
					'kelas'=> $this->input->post('kelas')
				);
				$this->m_crud->insert($tables,$data);
				$this->session->set_flashdata('success', '1');
				redirect($this->folder.'/'.$subfolder);
			}
		}
		
		// EDIT
		if(isset($_POST['edit'])){
			$tingkat_kelas = $this->input->post('tingkat_kelas');
			$kelas = $this->input->post('kelas');
			$check_kelas = $this->m_crud->getW($tables,array('tingkat_kelas'=>$tingkat_kelas,'kelas'=>$kelas));
			// duplicate found
			if($check_kelas->num_rows()>=1){
				$this->session->set_flashdata('duplicate', '1');
				redirect($this->folder.'/'.$subfolder);
			}
			// duplicate not found
			else{
				$id = $this->input->post('id');
				$data = array(
					'tingkat_kelas'=> $this->input->post('tingkat_kelas'),
					'kelas'=> $this->input->post('kelas')
				);
				$this->m_crud->update($tables, $data, $pk, $id);
				$this->session->set_flashdata('success', '1');
				redirect($this->folder.'/'.$subfolder);
			}
		}
		
		// DELETE
		if(isset($_POST['delete'])){
			$id = $this->input->post('id');
			$this->m_crud->delete($tables,$pk,$id);
			$this->session->set_flashdata('delete', '1');
			redirect($this->folder.'/'.$subfolder);
		}
						
		// VIEW
		else{
			$data['subtitle'] = "Kelas";
			$data['record'] = $this->m_crud->getAll($tables)->result();
			$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
		}
	}
	
	/****************************
	Agama
	*****************************/
	function app_agama(){
		// var
		$tables = $this->tables_agama;
		$pk = "id_agama";
		$subfolder = "app_agama";
		$data['title'] = $this->title;
		
		// POST
		if(isset($_POST['post'])){
			$data = array('agama'=> $this->input->post('agama'));
			$this->m_crud->insert($tables,$data);
			$this->session->set_flashdata('success', '1');
			redirect($this->folder.'/'.$subfolder);
		}
		
		// EDIT
		if(isset($_POST['edit'])){
			$id = $this->input->post('id');
			$data = array('agama'=> $this->input->post('agama'));
			$this->m_crud->update($tables, $data, $pk, $id);
			$this->session->set_flashdata('success', '1');
			redirect($this->folder.'/'.$subfolder);
		}
		
		// DELETE
		if(isset($_POST['delete'])){
			$id = $this->input->post('id');
			$this->m_crud->delete($tables,$pk,$id);
			$this->session->set_flashdata('delete', '1');
			redirect($this->folder.'/'.$subfolder);
		}
			
		// VIEW
		else{
			$data['subtitle'] = "Agama";
			$data['record'] = $this->m_crud->getAll($tables)->result();
			$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
		}
	}

	
}