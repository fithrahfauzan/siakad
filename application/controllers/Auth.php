<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class auth extends CI_Controller{
	
	// var db tables
	var $tables_admin = "app_admin";
	var $tables_guru = "master_guru";
	var $tables_siswa = "master_siswa";
	
	function index(){
		redirect('auth/login');
	}
	
	// LOGIN FORM
	function login(){
		$this->load->view('login');
	}
	
	// LOGIN AUTH
	function login_act(){
		$user = $this->input->post('user');
		$pass = $this->input->post('pass');
		
		// ADMIN
		$check_admin = $this->m_crud->getW($this->tables_admin,array(
			'username'=>$user,
			'password'=>md5($pass)
		));
		if($check_admin->num_rows()>0){
			$r = $check_admin->row_array();
			$data = array(
				'id_user' => $r['id_admin'],
				'level' => 'Admin'
			);
			$this->session->set_userdata($data);
			redirect('beranda');
		}
		else{
			// GURU
			$check_guru = $this->m_crud->getW($this->tables_guru,array(
				'kode_guru'=>$user,
				'password'=>md5($pass)
			));
			if($check_guru->num_rows()>0){
				$r = $check_guru->row_array();
				$data = array(
					'id_user' => $r['id_guru'],
					'level' => 'Guru'
				);
				$this->session->set_userdata($data);
				redirect('beranda');
			}
			else{
				// SISWA
				$check_siswa = $this->m_crud->getW($this->tables_siswa,array(
					'nis'=>$user,
					'password'=>md5($pass)
				));
				if($check_siswa->num_rows()>0){
					$r = $check_siswa->row_array();
					$data = array(
						'id_user' => $r['id_siswa'],
						'level' => 'Siswa'
					);
					$this->session->set_userdata($data);
					redirect('beranda');
				}
				
				// USER NOT FOUND
				else{
					redirect('auth/login/error');
				}
			}			
		}
	}
	
	// LOGOUT
	function logout(){
        $this->session->sess_destroy();
        redirect('');
    }
}
?>