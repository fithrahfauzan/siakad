<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class beranda extends CI_Controller{
	
	var $folder = "beranda";
	var $title = "Beranda";
	
	// var db tables
	var $tables_guru = "master_guru";
	var $tables_siswa = "master_siswa";
	var $tables_matpel = "akademik_matpel";
	var $tables_agama = "app_agama";
	var $tables_app_kelas = "app_kelas";
	var $tables_admin = "app_admin";
	
	function index(){
		// EDIT PASS
		if(isset($_POST['edit_pass'])){
			$tables = $this->input->post('tables');
			$pk = $this->input->post('pk');
			$id = $this->input->post('id');
			$old_pass = $this->input->post('old_pass');
			$pass = $this->input->post('pass');
			$pass_check = $this->input->post('pass_check');
			
			$check_pass = $this->m_crud->getW($tables,array($pk=>$id,'password'=>MD5($old_pass)));
			if($check_pass->num_rows()==0){
				$this->session->set_flashdata('error_old_pass', '1');
				redirect_back();
			}
			else if($pass != $pass_check){
				$this->session->set_flashdata('error_pass_check', '1');
				redirect_back();
			}
			else{
				$this->m_crud->update($tables, array('password'=>MD5($pass)), $pk, $id);
				$this->session->set_flashdata('success_pass', '1');
				redirect_back();
			}
		}
		// escape session
		else{
			if($this->session->userdata('level')=="Admin"){
				redirect('beranda/admin');
			}
			if($this->session->userdata('level')=="Guru"){
				redirect('beranda/guru');
			}
			if($this->session->userdata('level')=="Siswa"){
				redirect('beranda/siswa');
			}
		}
	}
	
	/****************************
	Beranda Admin
	*****************************/
	function admin(){
		// escape session
		if($this->session->userdata('level')!="Admin"){
			redirect_back();
		}
		// var		
		$tables = $this->tables_admin;
		$pk = 'id_admin';
		$file = "admin.php";
		$data['title'] = $this->title;		
		$id_user = $this->session->userdata('id_user');
		
		// VIEW	
		// get id_user
		$data['id_user'] = $this->session->userdata('id_user');
		
		// get siswa
		$data['siswa'] = $this->m_crud->getAll($this->tables_siswa);
		$data['siswa_l'] = $this->m_crud->getW($this->tables_siswa,array('jns_kelamin'=>'L'));
		$data['siswa_p'] = $this->m_crud->getW($this->tables_siswa,array('jns_kelamin'=>'P'));
		
		
		// get guru
		$data['guru'] = $this->m_crud->getAll($this->tables_guru);
		$data['guru_l'] = $this->m_crud->getW($this->tables_guru,array('jns_kelamin'=>'L'));
		$data['guru_p'] = $this->m_crud->getW($this->tables_guru,array('jns_kelamin'=>'P'));
		
		
		$data['subtitle'] = "Ruang Admin";		
		$this->template->load('theme', $this->folder.'/'.$file,$data);
	}
	
	/****************************
	Beranda Guru
	*****************************/
	function guru(){
		// escape session
		if($this->session->userdata('level')!="Guru"){
			redirect_back();
		}
		// var		
		$tables = $this->tables_guru;
		$pk = 'id_guru';
		$file = "guru.php";
		$data['title'] = $this->title;		
		$id_user = $this->session->userdata('id_user');
		
		// EDIT FOTO
		if(isset($_POST['edit_foto'])){
			$id = $this->input->post('id');
			$config['upload_path'] = './assets/img/foto/guru/';
			$config['allowed_types'] = 'jpg';
			$config['max_size'] = '3048'; 
			$config['overwrite'] = true;
			$config['file_name'] = 'guru'.'_'.$id;
			
			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload('foto')){
				$error_msg = $this->upload->display_errors('','');
				$this->session->set_flashdata('error', $error_msg);
				$this->session->set_flashdata('success', '1');
			}
			else{
				$dataimg = $this->upload->data();
				$foto = array('foto' => $dataimg['file_name']);
				$this->m_crud->update($tables, $foto, $pk, $id);
				$this->session->set_flashdata('success', '1');
			}
			redirect_back();
		}
		
		// EDIT PROFILE
		if(isset($_POST['edit_profile'])){
			$id = $this->input->post('id');
			$jalan = $this->input->post('jalan');
			$rt	= $this->input->post('rt');
			$rw	= $this->input->post('rw');
			$nomor = $this->input->post('nomor');
			$kel = $this->input->post('kel');
			$kec = $this->input->post('kec');
			$kab = $this->input->post('kab');
			$prov = $this->input->post('prov');
			$data = array(
				'tempat_lahir'  => $this->input->post('tempat_lahir'),
				'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
				'id_agama'		=> $this->input->post('agama'),
				'gol_darah'		=> $this->input->post('gol_darah'),
				'alamat'		=> "$jalan|$rt|$rw|$nomor|$kel|$kec|$kab|$prov",
				'email'			=> $this->input->post('email'),
				'telp' 			=> $this->input->post('telp')
			);
			$this->m_crud->update($tables, $data, $pk, $id);
			$this->session->set_flashdata('success', '1');
			redirect_back();
		}
						
		// VIEW
		else{
			$query = "
				SELECT
					id_guru, kode_guru, nip, nama, matpel, tempat_lahir, tanggal_lahir, jns_kelamin, agama, gol_darah, alamat, email, telp, foto, $tables.id_agama
				FROM $tables
				INNER JOIN $this->tables_matpel
					ON $this->tables_matpel.id_matpel = $tables.pddkn_umum
				INNER JOIN $this->tables_agama
					ON $this->tables_agama.id_agama = $tables.id_agama
				WHERE $pk = $id_user
			";
			
			$data['subtitle'] = "Ruang Guru";
			$data['r'] = $this->m_crud->normal($query)->row();
			$this->template->load('theme', $this->folder.'/'.$file,$data);
		}
	}
	
	/****************************
	Beranda Siswa
	*****************************/
	function siswa(){
		// escape session
		if($this->session->userdata('level')!="Siswa"){
			redirect_back();
		}
		// var
		$tables = $this->tables_siswa;
		$pk = 'id_siswa';
		$file = "siswa.php";
		$data['title'] = $this->title;		
		$id_user = $this->session->userdata('id_user');
		
		// EDIT FOTO
		if(isset($_POST['edit_foto'])){
			$id = $this->input->post('id');
			$config['upload_path'] = './assets/img/foto/siswa/';
			$config['allowed_types'] = 'jpg';
			$config['max_size'] = '3048'; 
			$config['overwrite'] = true;
			$config['file_name'] = 'siswa'.'_'.$id;
			
			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload('foto')){
				$error_msg = $this->upload->display_errors('','');
				$this->session->set_flashdata('error', $error_msg);
				$this->session->set_flashdata('success', '1');
			}
			else{
				$dataimg = $this->upload->data();
				$foto = array('foto' => $dataimg['file_name']);
				$this->m_crud->update($tables, $foto, $pk, $id);
				$this->session->set_flashdata('success', '1');
			}
			redirect_back();
		}
		
		// EDIT PROFILE
		if(isset($_POST['edit_profile'])){
			$id = $this->input->post('id');
			$jalan = $this->input->post('jalan');
			$rt	= $this->input->post('rt');
			$rw	= $this->input->post('rw');
			$nomor = $this->input->post('nomor');
			$kel = $this->input->post('kel');
			$kec = $this->input->post('kec');
			$kab = $this->input->post('kab');
			$prov = $this->input->post('prov');
			$data = array(
				'tempat_lahir'  => $this->input->post('tempat_lahir'),
				'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
				'id_agama'		=> $this->input->post('agama'),
				'gol_darah'		=> $this->input->post('gol_darah'),
				'alamat'		=> "$jalan|$rt|$rw|$nomor|$kel|$kec|$kab|$prov",
				'telp' 			=> $this->input->post('telp')
			);
			$this->m_crud->update($tables, $data, $pk, $id);
			$this->session->set_flashdata('success', '1');
			redirect_back();
		}
		
		// VIEW
		else{
			$query = "
				SELECT
					id_siswa, nis, nama, tempat_lahir, tanggal_lahir, jns_kelamin, agama, gol_darah, alamat, telp, foto, $tables.id_agama
				FROM $tables
				INNER JOIN $this->tables_agama
					ON $this->tables_agama.id_agama = $tables.id_agama
				WHERE $pk = $id_user
			";
			
			$data['tingkat_kelas'] = $this->m_crud->getW($this->tables_app_kelas,array('id_datakelas'=>user_info('id_kelas')))->row('tingkat_kelas');
			$data['subtitle'] = "Ruang Siswa";
			$data['r'] = $this->m_crud->normal($query)->row();
			$this->template->load('theme', $this->folder.'/'.$file,$data);
		}		
	}
	
}
?>