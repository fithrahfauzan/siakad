<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class master extends CI_Controller{
	
	var $folder = "master";
	var $title  = "Master Data";
	
	var $npsn = "20228655";
	
	// var db tables
	var $tables_guru = "master_guru";
	var $tables_siswa = "master_siswa";
	var $tables_kelas = "master_kelas";
	var $tables_kelas_sub = "master_kelas_detail";
	var $tables_tahun_ajar = "akademik_tahun_ajar";
	var $tables_matpel = "akademik_matpel";
	var $tables_agama = "app_agama";	
	
	function __construct(){
		parent::__construct();
		if($this->session->userdata('level')!="Admin"){
			redirect_back();
		}
	}
	
	function index(){
		redirect('master/guru');
	}
	
	/****************************
	Master Data Guru
	*****************************/
	function guru(){
		// var
		$tables = $this->tables_guru;
		$pk = "id_guru";
		$subfolder = "guru";
		$data['title'] = $this->title;
		
		// POST
		if($this->uri->segment(3)=="post"){
			//submit
			if(isset($_POST['submit'])){
				$nip = $this->input->post('nip');
				$check_nip = $this->m_crud->getW($tables,array('nip'=>$nip));
				// duplicate nip found
				if($check_nip->num_rows()>=1 && $nip != ""){
					$this->session->set_flashdata('duplicate', '1');
					redirect_back();
				}
				// duplicate nip not found
				else{
					$query = "SHOW TABLE STATUS LIKE '$tables'";
					$idq = $this->m_crud->normal($query)->row_array();
					$id = $idq['Auto_increment'];
					
					$reinput		= $this->input->post('reinput');
					
					$jalan			= $this->input->post('jalan');
					$rt				= $this->input->post('rt');
					$rw				= $this->input->post('rw');
					$nomor			= $this->input->post('nomor');
					$kel			= $this->input->post('kel');
					$kec			= $this->input->post('kec');
					$kab			= $this->input->post('kab');
					$prov			= $this->input->post('prov');
					
					$kode_guru		= $this->npsn."G".$id;
					
					$data = array(
						'kode_guru'		=> $kode_guru,
						'nip'     		=> $nip,
						'password'     	=> md5($this->input->post('tanggal_lahir')),
						'nama'     		=> $this->input->post('nama'),
						'pddkn_umum'    => $this->input->post('pddkn_umum'),
						'tempat_lahir'  => $this->input->post('tempat_lahir'),
						'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
						'jns_kelamin'	=> $this->input->post('jns_kelamin'),
						'id_agama'		=> $this->input->post('agama'),
						'gol_darah'		=> $this->input->post('gol_darah'),
						'alamat'		=> "$jalan|$rt|$rw|$nomor|$kel|$kec|$kab|$prov",
						'email'			=> $this->input->post('email'),
						'telp' 			=> $this->input->post('telp'),
						'foto'			=> ''
					);
					
					// save tanpa foto
					if (empty($_FILES['foto']['name'])){
						$this->m_crud->insert($tables,$data);
						$this->session->set_flashdata('warning', '1');
						if($reinput == 1){
							redirect($this->folder.'/'.$subfolder.'/post');
						}
						else{
							redirect($this->folder.'/'.$subfolder);
						}
					}
					// upload
					else{
						$config['upload_path'] = './assets/img/foto/guru/';
						$config['allowed_types'] = 'jpg';
						$config['max_size'] = '3048'; 
						$config['overwrite'] = true;
						$config['file_name'] = $subfolder.'_'.$id;
						
						$this->upload->initialize($config);
						
						// upload gagal
						if ( ! $this->upload->do_upload('foto')){
							$error_msg = $this->upload->display_errors('','');
							$this->session->set_flashdata('error', $error_msg);
							redirect_back();
						}
						// upload success
						else{
							$dataimg = $this->upload->data();
							$foto = array('foto' => $dataimg['file_name']);
							$data['foto'] = $foto['foto'];
							$this->m_crud->insert($tables,$data);
							$this->session->set_flashdata('success', '1');
							if($reinput == 1){
								redirect($this->folder.'/'.$subfolder.'/post');
							}
							else{
								redirect($this->folder.'/'.$subfolder);
							}
						}
					}	
				}
			}
			// page
			else{
				$data['subtitle'] = "Data Guru ::: Input Data Baru";
				$this->template->load('theme', $this->folder.'/'.$subfolder.'/post',$data);
			}
		}
		
		// EDIT
		else if($this->uri->segment(3)=="edit"){
			// submit
			if(isset($_POST['submit'])){
				$cur_nip = $this->input->post('cur_nip');
				$nip = $this->input->post('nip');
				$check_nip = $this->m_crud->getW($tables,array('nip'=>$nip));
				// duplicate nip found
				if($check_nip->num_rows()>=1 && $nip != $cur_nip && $nip != ""){
					$this->session->set_flashdata('duplicate', '1');
					redirect_back();
				}
				// duplicate nip not found
				else{
					$id = $this->input->post('id');
					if (empty($_FILES['foto']['name'])){
						$this->session->set_flashdata('warning', '1');
					}
					else{
						$config['upload_path'] = './assets/img/foto/guru/';
						$config['allowed_types'] = 'jpg';
						$config['max_size'] = '3048'; 
						$config['overwrite'] = true;
						$config['file_name'] = $subfolder.'_'.$id;
						
						$this->upload->initialize($config);
						
						if ( ! $this->upload->do_upload('foto')){
							$error_msg = $this->upload->display_errors('','');
							$this->session->set_flashdata('error', $error_msg);
							$this->session->set_flashdata('success', '1');
						}
						else{
							$dataimg = $this->upload->data();
							$foto = array('foto' => $dataimg['file_name']);
							$this->m_crud->update($tables, $foto, $pk, $id);
							$this->session->set_flashdata('success', '1');
						}
					}
					
					$jalan			= $this->input->post('jalan');
					$rt				= $this->input->post('rt');
					$rw				= $this->input->post('rw');
					$nomor			= $this->input->post('nomor');
					$kel			= $this->input->post('kel');
					$kec			= $this->input->post('kec');
					$kab			= $this->input->post('kab');
					$prov			= $this->input->post('prov');
					
					$data = array(
						'nip'     		=> $nip,
						'nama'     		=> $this->input->post('nama'),
						'pddkn_umum'    => $this->input->post('pddkn_umum'),
						'tempat_lahir'  => $this->input->post('tempat_lahir'),
						'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
						'jns_kelamin'	=> $this->input->post('jns_kelamin'),
						'id_agama'		=> $this->input->post('agama'),
						'gol_darah'		=> $this->input->post('gol_darah'),
						'alamat'		=> "$jalan|$rt|$rw|$nomor|$kel|$kec|$kab|$prov",
						'email'			=> $this->input->post('email'),
						'telp' 			=> $this->input->post('telp')
					);
					$this->m_crud->update($tables, $data, $pk, $id);
					redirect_back();
				}
			}
			// page
			else{
				// escape
				$check_id = $this->m_crud->getByID($tables,$pk,$this->uri->segment(4))->num_rows();
				if($this->uri->segment(4) == ""){
					redirect_back();
				}
				if($check_id==0){
					redirect_back();
				}
				$id = $this->uri->segment(4);
				$data['subtitle'] = "Data Guru ::: Edit Record #".$id;
				$data['r'] = $this->m_crud->getByID($tables,$pk,$id)->row_array();
				$this->template->load('theme', $this->folder.'/'.$subfolder.'/edit',$data);
			}
		}
				
		else{
			
			// DELETE
			if(isset($_POST['delete'])){
				$id = $this->input->post('id');
				$this->m_crud->delete($tables,$pk,$id);
				unlink('./assets/img/foto/guru/guru_'.$id.'.jpg');
				$this->session->set_flashdata('delete', '1');
				redirect($this->folder.'/'.$subfolder);
			}
			
			// VIEW
			else{
				$query = "
					SELECT
						id_guru, kode_guru, nip, nama, matpel, tempat_lahir, tanggal_lahir, jns_kelamin, agama, gol_darah, alamat, email, telp, foto
					FROM $tables
					INNER JOIN $this->tables_matpel
						ON $this->tables_matpel.id_matpel = $tables.pddkn_umum
					INNER JOIN $this->tables_agama
						ON $this->tables_agama.id_agama = $tables.id_agama
				";
				$data['subtitle'] = "Data Guru";
				$data['record'] = $this->m_crud->normal($query)->result();
				$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
			}
		}
	}
	
	/****************************
	Master Data Siswa
	*****************************/
	function siswa(){
		// var
		$tables = $this->tables_siswa;
		$pk = "id_siswa";
		$subfolder = "siswa";
		$data['title'] = $this->title;
		
		// POST
		if($this->uri->segment(3)=="post"){
			// submit
			if(isset($_POST['submit'])){
				$nis = $this->input->post('nis');
				$check_nis = $this->m_crud->getW($tables,array('nis'=>$nis));
				if($check_nis->num_rows()>=1){
					$this->session->set_flashdata('duplicate', '1');
					redirect_back();
				}
				else{
					$reinput		= $this->input->post('reinput');
					
					$jalan			= $this->input->post('jalan');
					$rt				= $this->input->post('rt');
					$rw				= $this->input->post('rw');
					$nomor			= $this->input->post('nomor');
					$kel			= $this->input->post('kel');
					$kec			= $this->input->post('kec');
					$kab			= $this->input->post('kab');
					$prov			= $this->input->post('prov');
					
					
					$data = array(
						'nis'     		=> $nis,
						'password'     	=> md5($this->input->post('tanggal_lahir')),
						'nama'     		=> $this->input->post('nama'),
						'tempat_lahir'  => $this->input->post('tempat_lahir'),
						'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
						'jns_kelamin'	=> $this->input->post('jns_kelamin'),
						'id_agama'		=> $this->input->post('agama'),
						'gol_darah'		=> $this->input->post('gol_darah'),
						'alamat'		=> "$jalan|$rt|$rw|$nomor|$kel|$kec|$kab|$prov",
						'telp' 			=> $this->input->post('telp'),
						'foto'			=> ""
					);
					
					// save tanpa foto
					if (empty($_FILES['foto']['name'])){
						$this->m_crud->insert($tables,$data);
						$this->session->set_flashdata('warning', '1');
						if($reinput == 1){
							redirect($this->folder.'/'.$subfolder.'/post');
						}
						else{
							redirect($this->folder.'/'.$subfolder);
						}
					}
					// upload
					else{
						$query = "SHOW TABLE STATUS LIKE '$tables'";
						$idq = $this->m_crud->normal($query)->row_array();
						$id = $idq['Auto_increment'];
						
						$config['upload_path'] = './assets/img/foto/siswa/';
						$config['allowed_types'] = 'jpg';
						$config['max_size'] = '3048'; 
						$config['overwrite'] = true;
						$config['file_name'] = $subfolder.'_'.$id;
						
						$this->upload->initialize($config);
						
						// upload error				
						if ( ! $this->upload->do_upload('foto')){
							$error_msg = $this->upload->display_errors('','');
							$this->session->set_flashdata('error', $error_msg);
							redirect_back();
						}
						// upload success
						else{						
							$dataimg = $this->upload->data();
							$foto = array('foto' => $dataimg['file_name']);
							$data['foto'] = $foto['foto'];
							$this->m_crud->insert($tables,$data);
							$this->session->set_flashdata('success', '1');
							if($reinput == 1){
								redirect($this->folder.'/'.$subfolder.'/post');
							}
							else{
								redirect($this->folder.'/'.$subfolder);
							}
						}
					}	
				}
			}
			// page
			else{
				$data['subtitle'] = "Data Siswa ::: Input Data Baru";
				$this->template->load('theme', $this->folder.'/'.$subfolder.'/post',$data);
			}
		}
		
		// EDIT
		else if($this->uri->segment(3)=="edit"){
			// submit
			if(isset($_POST['submit'])){
				$nis_b = $this->input->post('nis_b');
				$nis = $this->input->post('nis');
				$check_nis = $this->m_crud->getW($tables,array('nis'=>$nis));
				// duplicate nis found
				if($check_nis->num_rows()>=1 && $nis != $nis_b && $nis != ""){
					$this->session->set_flashdata('duplicate', '1');
					redirect_back();
				}
				// duplicate nis not found
				else{
					$id = $this->input->post('id');
					
					if (empty($_FILES['foto']['name'])){
						$this->session->set_flashdata('warning', '1');
					}
					else{
						$config['upload_path'] = './assets/img/foto/siswa/';
						$config['allowed_types'] = 'jpg';
						$config['max_size'] = '3048'; 
						$config['overwrite'] = true;
						$config['file_name'] = $subfolder.'_'.$id;
						
						$this->upload->initialize($config);
						
						if ( ! $this->upload->do_upload('foto')){
							$error_msg = $this->upload->display_errors('','');
							$this->session->set_flashdata('error', $error_msg);
							$this->session->set_flashdata('success', '1');
						}
						else{
							$dataimg = $this->upload->data();
							$foto = array('foto' => $dataimg['file_name']);
							$this->m_crud->update($tables, $foto, $pk, $id);
							$this->session->set_flashdata('success', '1');
						}
					}

					$jalan			= $this->input->post('jalan');
					$rt				= $this->input->post('rt');
					$rw				= $this->input->post('rw');
					$nomor			= $this->input->post('nomor');
					$kel			= $this->input->post('kel');
					$kec			= $this->input->post('kec');
					$kab			= $this->input->post('kab');
					$prov			= $this->input->post('prov');
					
					$data = array(
						'nis'			=> $nis,
						'nama'     		=> $this->input->post('nama'),
						'tempat_lahir'  => $this->input->post('tempat_lahir'),
						'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
						'jns_kelamin'	=> $this->input->post('jns_kelamin'),
						'id_agama'		=> $this->input->post('agama'),
						'gol_darah'		=> $this->input->post('gol_darah'),
						'alamat'		=> "$jalan|$rt|$rw|$nomor|$kel|$kec|$kab|$prov",
						'telp' 			=> $this->input->post('telp')
					);
					$this->m_crud->update($tables, $data, $pk, $id);
					redirect_back();
				}
			}
			// page
			else{
				// escape
				$check_id = $this->m_crud->getByID($tables,$pk,$this->uri->segment(4))->num_rows();
				if($this->uri->segment(4) == ""){
					redirect_back();
				}
				if($check_id==0){
					redirect_back();
				}
				$id = $this->uri->segment(4);
				$data['subtitle'] = "Data Siswa ::: Edit Record #".$id;
				$data['r'] = $this->m_crud->getByID($tables,$pk,$id)->row_array();
				$this->template->load('theme', $this->folder.'/'.$subfolder.'/edit',$data);
			}
		}
		
		else{
			
			// DELETE
			if(isset($_POST['delete'])){
				$id = $this->input->post('id');
				$this->m_crud->delete($tables,$pk,$id);
				unlink('./assets/img/foto/siswa/siswa_'.$id.'.jpg');
				$this->session->set_flashdata('delete', '1');
				redirect($this->folder.'/'.$subfolder);
			}
			
			// VIEW
			else{
				$query = "
					SELECT
						id_siswa, nis, nama, tempat_lahir, tanggal_lahir, jns_kelamin, agama, gol_darah, alamat, telp, foto
					FROM $tables
					INNER JOIN $this->tables_agama
						ON $this->tables_agama.id_agama = $tables.id_agama
				";
				$data['subtitle'] = "Data Siswa";
				$data['record'] = $this->m_crud->normal($query)->result();
				$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
			}
		}
	}
	
	/****************************
	Master Data Kelas
	*****************************/
	function kelas(){
		// var
		$tables = $this->tables_kelas;
		$subtables = $this->tables_kelas_sub;	
		$pk = "id_kelas";
		$subfolder = "kelas";
		$data['title'] = $this->title;		
		
		// SET SESSION MENU FILTER
		if(isset($_POST['session_filter'])){
			//generate db master_kelas
			if(isset($_POST['generate'])){
				$data = array(
					'id_kelas'=>'',
					'id_datakelas'=>$this->input->post('kelas'),
					'id_tahun_ajar'=>$this->input->post('tahun_ajar'),					
					'wali_kelas'=>''
				);				
				$this->m_crud->insert($tables,$data);
			}
			
			//filtering
			$data = array(
				'filtered' => '1',
				'kelas' => $this->input->post('kelas'),
				'tahun_ajar' => $this->input->post('tahun_ajar')				
			);
			$this->session->set_flashdata($data);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// CETAK
		else if(isset($_POST['cetak'])){									
			$data = array(
				'menu'=>'kelas',
				'id_kelas'=>$this->input->post('id_kelas')
			);
			$this->session->set_flashdata($data);
			redirect('laporan');
		}
		
		// POST SISWA
		else if(isset($_POST['post_siswa'])){
			$id_siswa = $this->input->post('id_siswa');
			$id_kelas = $this->input->post('id_kelas');
			$cur_menu = $this->m_crud->getW($tables,array('id_kelas'=>$id_kelas));
			
			if(empty($id_siswa)){
				// message
				$data_filter = array(
					'warning' => '1',
					'filtered' => '1',
					'kelas' => $cur_menu->row('id_datakelas'),
					'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
				);
			}
			else{											
				// insert
				$data = array();			
				foreach($id_siswa as $a){
					$row = array(
						'id_detailkelas'=>'',
						'id_kelas'=>$id_kelas,
						'id_siswa'=>$a
					);
					$data[] = $row;
				}
				$this->m_crud->insertBatch($subtables,$data);	
				
				// message			
				$data_filter = array(
					'success' => '1',
					'filtered' => '1',
					'kelas' => $cur_menu->row('id_datakelas'),
					'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
				);
			}
			
			$this->session->set_flashdata($data_filter);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// EDIT WALI
		else if(isset($_POST['edit_wali'])){
			$id_guru = $this->input->post('id_guru');
			$id_kelas = $this->input->post('id_kelas');
			$data = array('wali_kelas'=>$id_guru);
			
			$this->m_crud->update($tables, $data, $pk, $id_kelas);
			
			// message
			$cur_menu = $this->m_crud->getW($tables,array('id_kelas'=>$id_kelas));
			$data_filter = array(
				'success' => '1',
				'filtered' => '1',
				'kelas' => $cur_menu->row('id_datakelas'),
				'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
			);
			//print_r($data_filter);
			$this->session->set_flashdata($data_filter);
			redirect($this->folder.'/'.$subfolder.'/filter');
		}
		
		// REMOVE
		else if(isset($_POST['remove'])){
			$id = $this->input->post('id');
			$id_kelas = $this->input->post('id_kelas');
			$this->m_crud->delete($subtables,'id_detailkelas',$id);	
			
			// message
			$cur_menu = $this->m_crud->getW($tables,array('id_kelas'=>$id_kelas));
			$data_filter = array(
				'remove' => '1',
				'filtered' => '1',
				'kelas' => $cur_menu->row('id_datakelas'),
				'tahun_ajar' => $cur_menu->row('id_tahun_ajar')
			);
			$this->session->set_flashdata($data_filter);
			redirect($this->folder.'/'.$subfolder.'/filter');			
		}
		
		// VIEW
		else if($this->uri->segment(3)=="filter"){
			//escape
			if($this->session->flashdata('filtered')== ""){
				redirect($this->folder.'/'.$subfolder);
			}			
			else{
				$kelas = $this->session->flashdata('kelas');
				$tahun_ajar = $this->session->flashdata('tahun_ajar');				
				
				$data['checkkelasdb'] = $this->m_crud->getW($tables,array(
						'id_datakelas'=>$kelas,
						'id_tahun_ajar'=>$tahun_ajar
					));
				$data['id_masterkelas'] = $data['checkkelasdb']->row('id_kelas');
				
				$query = "
					SELECT 
						$tables.id_kelas, id_detailkelas, nis, nama, jns_kelamin
					FROM $tables
					INNER JOIN $subtables
						ON $subtables.id_kelas = $tables.id_kelas
					INNER JOIN $this->tables_siswa 
						ON $this->tables_siswa.id_siswa = $subtables.id_siswa
					WHERE 
						id_datakelas = '$kelas' AND 
						id_tahun_ajar = '$tahun_ajar'
					ORDER BY
						nama ASC
				";
				$data['datakelas'] = $this->m_crud->normal($query);
				
				// get nama kelas
				$data['kelas'] = id_datakelas($kelas);
				
				// get jumlah siswa
				$data['jumlah'] = $data['datakelas']->num_rows();
				$qjumlah = "SELECT SUM(jns_kelamin = 'L') AS jumlahL, SUM(jns_kelamin = 'P') AS jumlahP
				FROM ($query) AS tb
				WHERE jns_kelamin = 'L' OR jns_kelamin = 'P'";
				$djumlah = $this->m_crud->normal($qjumlah);
				$data['jumlahL'] = $djumlah->row('jumlahL');
				$data['jumlahP'] = $djumlah->row('jumlahP');
				
				// get cur menu filter
				$data['cur_idkelas'] = $kelas;
				$data['cur_idtahun'] = $tahun_ajar;
				$data['cur_tahun'] = $this->m_crud->getW($this->tables_tahun_ajar,array('id_tahun_ajar'=>$tahun_ajar))->row('tahun_ajar');

				// get wali kelas
				$wali_kelas = $data['checkkelasdb']->row('wali_kelas');
				$data['wali_kelas'] = $this->m_crud->getW($this->tables_guru,array('id_guru'=>$wali_kelas))->row('nama');			
				$data['id_wali_kelas'] = $data['checkkelasdb']->row('wali_kelas');
				
				// get siswa untuk post
				$query_siswa = "
					SELECT id_siswa, nis, nama, jns_kelamin
					FROM $this->tables_siswa WHERE id_siswa NOT IN 
					(SELECT id_siswa FROM $tables
					INNER JOIN $subtables ON $subtables.id_kelas = $tables.id_kelas
					WHERE id_tahun_ajar = '$tahun_ajar')
				";
				$data['siswa'] = $this->m_crud->normal($query_siswa)->result();							
				
				$data['subtitle'] = "Data Kelas";
				$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
			}
		}
			
		// MENU FILTER
		else{			
			$data['subtitle'] = "Data Kelas";
			$data['cur_idkelas'] = "";
			$data['cur_idtahun'] = $this->m_crud->getW($this->tables_tahun_ajar,array('aktif'=>'Y'))->row('tahun_ajar');
			
			$this->template->load('theme', $this->folder.'/'.$subfolder.'/view',$data);
		}
	}
}
?>