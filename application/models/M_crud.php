<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_crud extends CI_Model{
	
	// query biasa
	public function normal($query){
        return $this->db->query($query);
    }
	
    // menampilkan satu record berdasarkan parameter.
	public  function getW($tables,$data){
        return $this->db->get_where($tables,$data);
    }
    public  function getByID($tables,$pk,$id){
        $this->db->where($pk,$id);
        return $this->db->get($tables);
    }
	
	// menampilkan semua data dari sebuah tabel.
    public function getAll($tables){
        return $this->db->get($tables);
    }
	
	// memasukan data ke database.
    public function insert($tables,$data){
        $this->db->insert($tables,$data);
    }
	
	// memasukan multiple data
	public function insertBatch($tables,$data){
		$this->db->insert_batch($tables, $data); 
	}
	
	// update data kedalalam sebuah tabel
    public function update($tables,$data,$pk,$id){
        $this->db->where($pk,$id);
        $this->db->update($tables,$data);
    }
	
	// menghapus data dari sebuah tabel
    public function delete($tables,$pk,$id){
        $this->db->where($pk,$id);
        $this->db->delete($tables);
    }
	
	// menampilkan dengan sortir
	public function getAllSort($tables,$sort){
		$this->db->order_by($sort); 
        return $this->db->get($tables);
    }
	public  function getWSort($tables,$data,$sort){
		$this->db->order_by($sort); 
        return $this->db->get_where($tables,$data);
    }
	
}
?>