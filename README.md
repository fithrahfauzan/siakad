# Siakad

Sistem Informasi Akademik

### Prerequisites

* Deploy in PHP 5.4.7
* Cannot be installed in PHP 7.*

### Installing

To run this project do step :
* Run siakad.sql in /data/siakad.sql to new created database
* Change config in /application/config/database.php to your database config
* Change $config['base_url'] inside /application/config/config.php to your base url


After doing above step you can run this project, and login with :
* username: AdminSuper 
* password: rahasia

## Built With

* [Codeigniter 3.x](https://codeigniter.com/user_guide/) - The web framework used

## Authors

* **Fithrah Fauzan** - *Initial work* - [fithrah.fauzan@gmail.com](fithrah.fauzan@gmail.com)
