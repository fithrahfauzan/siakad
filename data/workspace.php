<NotepadPlus>
    <Project name="Sistem Informasi Akademik">
        <Folder name="controller">
            <File name="application\controllers\auth.php" />
            <File name="application\controllers\beranda.php" />
            <File name="application\controllers\master.php" />
            <File name="application\controllers\akademik.php" />
            <File name="application\controllers\config.php" />
            <File name="application\controllers\guru.php" />
            <File name="application\controllers\siswa.php" />
        </Folder>
        <Folder name="views">
            <Folder name="beranda">
                <File name="application\views\beranda\admin.php" />
                <File name="application\views\beranda\admin_modal_detail.php" />
                <File name="application\views\beranda\admin_modal_edit_pass.php" />
                <File name="application\views\beranda\guru.php" />
                <File name="application\views\beranda\guru_modal_edit.php" />
                <File name="application\views\beranda\guru_modal_edit_pass.php" />
                <File name="application\views\beranda\siswa.php" />
                <File name="application\views\beranda\siswa_modal_edit.php" />
                <File name="application\views\beranda\siswa_modal_edit_pass.php" />
            </Folder>
            <Folder name="master">
                <Folder name="guru">
                    <File name="application\views\master\guru\view.php" />
                    <File name="application\views\master\guru\post.php" />
                    <File name="application\views\master\guru\edit.php" />
                </Folder>
                <Folder name="siswa">
                    <File name="application\views\master\siswa\view.php" />
                    <File name="application\views\master\siswa\post.php" />
                    <File name="application\views\master\siswa\edit.php" />
                </Folder>
                <Folder name="kelas">
                    <File name="application\views\master\kelas\view.php" />
                    <File name="application\views\master\kelas\filter.php" />
                    <File name="application\views\master\kelas\modal_post.php" />
                    <File name="application\views\master\kelas\modal_edit.php" />
                    <File name="application\views\master\kelas\modal_delete.php" />
                </Folder>
            </Folder>
            <Folder name="akademik">
                <Folder name="tahun_ajar">
                    <File name="application\views\akademik\tahun_ajar\view.php" />
                    <File name="application\views\akademik\tahun_ajar\modal_post.php" />
                    <File name="application\views\akademik\tahun_ajar\modal_edit.php" />
                    <File name="application\views\akademik\tahun_ajar\modal_aktifkan.php" />
                    <File name="application\views\akademik\tahun_ajar\modal_delete.php" />
                </Folder>
                <Folder name="matpel">
                    <File name="application\views\akademik\matpel\view.php" />
                    <File name="application\views\akademik\matpel\modal_post.php" />
                    <File name="application\views\akademik\matpel\modal_edit.php" />
                    <File name="application\views\akademik\matpel\modal_delete.php" />
                </Folder>
                <Folder name="jadwal">
                    <File name="application\views\akademik\jadwal\view.php" />
                    <File name="application\views\akademik\jadwal\filter.php" />
                    <File name="application\views\akademik\jadwal\modal_post_hari.php" />
                    <File name="application\views\akademik\jadwal\modal_delete_hari.php" />
                    <File name="application\views\akademik\jadwal\modal_post_pelajaran.php" />
                    <File name="application\views\akademik\jadwal\modal_post_kegiatan.php" />
                    <File name="application\views\akademik\jadwal\modal_edit_pelajaran.php" />
                    <File name="application\views\akademik\jadwal\modal_edit_kegiatan.php" />
                    <File name="application\views\akademik\jadwal\modal_delete_jadwal.php" />
                </Folder>
            </Folder>
            <Folder name="config">
                <Folder name="app_kelas">
                    <File name="application\views\config\app_kelas\view.php" />
                    <File name="application\views\config\app_kelas\modal_post.php" />
                    <File name="application\views\config\app_kelas\modal_edit.php" />
                    <File name="application\views\config\app_kelas\modal_delete.php" />
                </Folder>
                <Folder name="app_agama">
                    <File name="application\views\config\app_agama\view.php" />
                    <File name="application\views\config\app_agama\modal_post.php" />
                    <File name="application\views\config\app_agama\modal_edit.php" />
                    <File name="application\views\config\app_agama\modal_delete.php" />
                </Folder>
            </Folder>
            <Folder name="guru">
                <Folder name="jadwal_mengajar">
                    <File name="application\views\guru\jadwal_mengajar\view.php" />
                </Folder>
            </Folder>
            <Folder name="siswa">
                <File name="application\views\siswa\jadwal_pelajaran\view.php" />
            </Folder>
            <File name="application\views\theme.php" />
            <File name="application\views\theme_header.php" />
            <File name="application\views\theme_footer.php" />
            <File name="application\views\login.php" />
            <File name="application\views\lorem.php" />
        </Folder>
        <Folder name="model">
            <File name="application\models\m_crud.php" />
        </Folder>
        <Folder name="libraries">
            <File name="application\libraries\template.php" />
        </Folder>
        <Folder name="helper">
            <File name="application\helpers\custom_helper.php" />
            <File name="application\helpers\My_url_helper.php" />
            <File name="application\helpers\alertc_helper.php" />
        </Folder>
        <Folder name="config">
            <File name="application\config\autoload.php" />
            <File name="application\config\config.php" />
            <File name="application\config\database.php" />
            <File name="application\config\routes.php" />
        </Folder>
        <Folder name="css">
            <File name="assets\css\login.css" />
            <File name="assets\css\style.css" />
        </Folder>
        <File name=".htaccess" />
    </Project>
</NotepadPlus>
