-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2016 at 09:48 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `siakad`
--

-- --------------------------------------------------------

--
-- Table structure for table `akademik_jadwal`
--

CREATE TABLE IF NOT EXISTS `akademik_jadwal` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `id_kelas` int(11) NOT NULL,
  `hari` enum('1','2','3','4','5','6','7') NOT NULL,
  PRIMARY KEY (`id_jadwal`),
  KEY `id_kelas` (`id_kelas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `akademik_jadwal`
--

INSERT INTO `akademik_jadwal` (`id_jadwal`, `id_kelas`, `hari`) VALUES
(1, 1, '1'),
(2, 1, '2'),
(3, 1, '3'),
(4, 1, '4'),
(5, 1, '5'),
(6, 1, '6');

-- --------------------------------------------------------

--
-- Table structure for table `akademik_jadwal_detail`
--

CREATE TABLE IF NOT EXISTS `akademik_jadwal_detail` (
  `id_jadwal_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_jadwal` int(11) NOT NULL,
  `waktu` varchar(100) NOT NULL,
  `matpel` int(11) NOT NULL,
  `kegiatan` varchar(100) NOT NULL,
  `pengajar` int(11) NOT NULL,
  PRIMARY KEY (`id_jadwal_detail`),
  KEY `id_jadwal` (`id_jadwal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `akademik_jadwal_detail`
--

INSERT INTO `akademik_jadwal_detail` (`id_jadwal_detail`, `id_jadwal`, `waktu`, `matpel`, `kegiatan`, `pengajar`) VALUES
(1, 1, '07:00 - 07:40', 0, 'Upacara Bendera', 0),
(2, 1, '07:40 - 08:20', 2, '', 11),
(3, 1, '08:20 - 09:00', 2, '', 11),
(4, 1, '09:00 - 09:40', 6, '', 11),
(5, 1, '09:40 - 10:20', 6, '', 11),
(6, 1, '10:20 - 10:40', 0, 'Istirahat', 0),
(7, 1, '10:40 - 11:20', 1, '', 13),
(8, 2, '07:00 - 07:15', 0, 'Apel', 0),
(9, 2, '07:15 - 07:55', 9, '', 14),
(10, 2, '07:55 - 08:35', 9, '', 14),
(11, 2, '08:35 - 09:15', 5, '', 11),
(12, 2, '09:15 - 09:55', 5, '', 11),
(13, 2, '09:55 - 10:25', 0, 'Istirahat', 0),
(14, 2, '10:25 - 11:05', 4, '', 11),
(15, 2, '11:05 - 11:45', 4, '', 11),
(16, 3, '07:00 - 07:15', 0, 'Apel', 0),
(17, 3, '07:15 - 07:55', 9, '', 14),
(18, 3, '07:55 - 08:35', 9, '', 14),
(19, 3, '08:35 - 09:15', 4, '', 11),
(20, 3, '09:15 - 09:55', 4, '', 11),
(21, 3, '09:55 - 10:25', 0, 'Istirahat', 0),
(22, 3, '10:25 - 11:05', 5, '', 11),
(23, 3, '11:05 - 11:45', 5, '', 11),
(24, 4, '07:00 - 07:15', 0, 'Apel', 0),
(25, 4, '07:15 - 07:55', 2, '', 11),
(26, 4, '07:55 - 08:35', 2, '', 11),
(27, 4, '08:35 - 09:15', 1, '', 13),
(28, 4, '09:15 - 09:55', 1, '', 13),
(29, 4, '09:55 - 10:25', 0, 'Istirahat', 0),
(30, 4, '10:25 - 11:05', 6, '', 11),
(31, 4, '11:05 - 11:45', 6, '', 11),
(32, 5, '07:00 - 07:15', 0, 'Apel', 0),
(33, 5, '07:15 - 07:55', 0, 'Jum''at Bersih', 0),
(34, 5, '07:55 - 08:35', 7, '', 15),
(35, 5, '08:35 - 09:15', 7, '', 15),
(36, 5, '09:40 - 10:20', 8, '', 16),
(37, 5, '10:20 - 11:00', 3, '', 12),
(38, 5, '09:15 - 09:40', 0, 'Istirahat', 0),
(39, 6, '07:00 - 07:15', 0, 'Apel', 0),
(40, 6, '07:15 - 07:55', 2, '', 11),
(41, 6, '07:55 - 08:35', 2, '', 11),
(42, 6, '08:35 - 09:15', 1, '', 14),
(43, 6, '09:15 - 09:55', 1, '', 14),
(44, 6, '09:55 - 10:25', 0, 'Istirahat', 0),
(45, 6, '10:25 - 11:05', 3, '', 12);

-- --------------------------------------------------------

--
-- Table structure for table `akademik_matpel`
--

CREATE TABLE IF NOT EXISTS `akademik_matpel` (
  `id_matpel` int(11) NOT NULL AUTO_INCREMENT,
  `matpel` varchar(50) NOT NULL,
  PRIMARY KEY (`id_matpel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `akademik_matpel`
--

INSERT INTO `akademik_matpel` (`id_matpel`, `matpel`) VALUES
(1, 'Matematika'),
(2, 'Bahasa Indonesia'),
(3, 'Bahasa Inggris'),
(4, 'P K n'),
(5, 'I P A'),
(6, 'I P S'),
(7, 'Penjaskes'),
(8, 'Pendidikan Agama'),
(9, 'S B K');

-- --------------------------------------------------------

--
-- Table structure for table `akademik_tahun_ajar`
--

CREATE TABLE IF NOT EXISTS `akademik_tahun_ajar` (
  `id_tahun_ajar` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_ajar` varchar(50) NOT NULL,
  `aktif` enum('T','Y') NOT NULL DEFAULT 'T',
  `semester_aktif` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tahun_ajar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `akademik_tahun_ajar`
--

INSERT INTO `akademik_tahun_ajar` (`id_tahun_ajar`, `tahun_ajar`, `aktif`, `semester_aktif`) VALUES
(1, '2015/2016', 'Y', 'Ganjil');

-- --------------------------------------------------------

--
-- Table structure for table `app_admin`
--

CREATE TABLE IF NOT EXISTS `app_admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `app_admin`
--

INSERT INTO `app_admin` (`id_admin`, `username`, `password`, `nama`) VALUES
(1, 'adminSuper', 'ac43724f16e9241d990427ab7c8f4228', 'Super Admin');

-- --------------------------------------------------------

--
-- Table structure for table `app_agama`
--

CREATE TABLE IF NOT EXISTS `app_agama` (
  `id_agama` int(11) NOT NULL AUTO_INCREMENT,
  `agama` varchar(30) NOT NULL,
  PRIMARY KEY (`id_agama`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `app_agama`
--

INSERT INTO `app_agama` (`id_agama`, `agama`) VALUES
(1, 'Islam'),
(2, 'Kristen'),
(3, 'Katolik'),
(4, 'Budha'),
(5, 'Hindhu'),
(6, 'Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `app_kelas`
--

CREATE TABLE IF NOT EXISTS `app_kelas` (
  `id_datakelas` int(11) NOT NULL AUTO_INCREMENT,
  `tingkat_kelas` enum('1','2','3','4','5','6') NOT NULL,
  `kelas` varchar(50) NOT NULL,
  PRIMARY KEY (`id_datakelas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `app_kelas`
--

INSERT INTO `app_kelas` (`id_datakelas`, `tingkat_kelas`, `kelas`) VALUES
(1, '1', 'A'),
(2, '2', 'A'),
(3, '3', 'A'),
(4, '4', 'A'),
(5, '5', 'A'),
(6, '6', 'A'),
(7, '1', 'B');

-- --------------------------------------------------------

--
-- Table structure for table `app_mainmenu`
--

CREATE TABLE IF NOT EXISTS `app_mainmenu` (
  `id_mainmenu` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_mainmenu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `app_mainmenu`
--

INSERT INTO `app_mainmenu` (`id_mainmenu`, `nama`, `icon`, `link`, `level`) VALUES
(1, 'Beranda', 'fa fa-home', 'beranda/admin', 'Admin'),
(2, 'Master Data', 'fa fa-database', 'master', 'Admin'),
(3, 'Akademik', 'fa fa-graduation-cap', 'akademik', 'Admin'),
(4, 'Config', 'fa fa-cogs', 'config', 'Admin'),
(20, 'Beranda', 'fa fa-home', 'beranda/guru', 'Guru'),
(21, 'Jadwal Mengajar', 'fa fa-calendar', 'guru/jadwal_mengajar', 'Guru'),
(31, 'Beranda', 'fa fa-home', 'beranda/siswa', 'Siswa'),
(32, 'Jadwal Pelajaran', 'fa fa-calendar', 'siswa/jadwal_pelajaran', 'Siswa');

-- --------------------------------------------------------

--
-- Table structure for table `app_setting`
--

CREATE TABLE IF NOT EXISTS `app_setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(100) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `app_setting`
--

INSERT INTO `app_setting` (`id_setting`, `item`, `content`) VALUES
(1, 'nama_sekolah', 'SD Negeri Cikumpa'),
(2, 'welcome_msg', 'Selamat datang di fasilitas Sistem Informasi Akademik. Fasilitas ini merupakan salah satu bentuk pelayanan dari pihak pengelola sekolah dalam menyediakan informasi akademik. Dan diharapkan semua yang menggunakan fasilitas ini dapat memperoleh informasi tersebut dengan mudah.');

-- --------------------------------------------------------

--
-- Table structure for table `app_submenu`
--

CREATE TABLE IF NOT EXISTS `app_submenu` (
  `id_submenu` int(11) NOT NULL AUTO_INCREMENT,
  `id_mainmenu` int(3) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_submenu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `app_submenu`
--

INSERT INTO `app_submenu` (`id_submenu`, `id_mainmenu`, `nama`, `icon`, `link`, `level`) VALUES
(1, 2, 'Data Guru', 'gi gi-parents', 'guru', 'Admin'),
(2, 2, 'Data Siswa', 'gi gi-group', 'siswa', 'Admin'),
(3, 2, 'Data Kelas', 'gi gi-bank', 'kelas', 'Admin'),
(4, 3, 'Tahun Ajaran', 'gi gi-calendar', 'tahun_ajar', 'Admin'),
(5, 3, 'Mata Pelajaran', 'fa fa-book', 'matpel', 'Admin'),
(6, 3, 'Jadwal Pelajaran', 'fa fa-calendar', 'jadwal', 'Admin'),
(8, 4, 'Kelas', 'fa fa-gear', 'app_kelas', 'Admin'),
(9, 4, 'Agama', 'fa fa-gear', 'app_agama', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `master_guru`
--

CREATE TABLE IF NOT EXISTS `master_guru` (
  `id_guru` int(11) NOT NULL AUTO_INCREMENT,
  `kode_guru` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `pddkn_umum` int(2) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jns_kelamin` enum('L','P') NOT NULL DEFAULT 'L',
  `id_agama` int(2) NOT NULL DEFAULT '1',
  `gol_darah` enum('Not Set','O','A','B','AB') NOT NULL,
  `alamat` varchar(300) NOT NULL COMMENT 'jalan/desa|rt|rw|norumah|kel|kec|kab/kota|prov',
  `email` varchar(50) NOT NULL,
  `telp` varchar(50) NOT NULL,
  `foto` varchar(150) NOT NULL,
  PRIMARY KEY (`id_guru`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `master_guru`
--

INSERT INTO `master_guru` (`id_guru`, `kode_guru`, `nip`, `password`, `nama`, `pddkn_umum`, `tempat_lahir`, `tanggal_lahir`, `jns_kelamin`, `id_agama`, `gol_darah`, `alamat`, `email`, `telp`, `foto`) VALUES
(11, '20228655G11', '03447641245', 'be460fd334b4fc86356d3f7783c745c2', 'Wahyu Widiastuti, S.Pd', 2, 'Jakarta', '1975-07-17', 'P', 1, 'O', 'Jl. H. Maih|||||||', 'cybercrowz@gmail.com', '08994239819', 'guru_11.jpg'),
(12, '20228655G12', '37357426433', '23f649e9a0bd678ef867f936ff580540', 'Sukismi, S.Pd', 3, 'Sukabumi', '1964-07-29', 'P', 1, 'Not Set', '|||||||', '', '', ''),
(13, '20228655G13', '62397486512', 'be460fd334b4fc86356d3f7783c745c2', 'Opar Suparma, M.Si', 5, 'Jakarta', '1975-07-17', 'L', 1, 'Not Set', '|||||||', '', '', ''),
(14, '20228655G14', '34607626633', 'be460fd334b4fc86356d3f7783c745c2', 'Dian Aggraeni, S.Pd', 9, 'Bogor', '1975-07-17', 'P', 1, 'Not Set', '|||||||', '', '', ''),
(15, '20228655G15', '43587476493', 'be460fd334b4fc86356d3f7783c745c2', 'Nurkomar, S.Pd', 7, 'Sukabumi', '1975-07-17', 'P', 1, 'Not Set', '|||||||', '', '', 'guru_15.jpg'),
(16, '20228655G16', '72557636643', 'be460fd334b4fc86356d3f7783c745c2', 'Nuri Safaiah, S.Ag', 8, 'Jakarta', '1975-07-17', 'L', 1, 'Not Set', '|||||||', '', '', ''),
(17, '20228655G17', '', 'be460fd334b4fc86356d3f7783c745c2', 'Mardiah, Ama.Pd', 6, 'Jakarta', '1975-07-17', 'P', 1, 'Not Set', '|||||||', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `master_kelas`
--

CREATE TABLE IF NOT EXISTS `master_kelas` (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `id_datakelas` int(11) NOT NULL,
  `id_tahun_ajar` int(4) NOT NULL,
  `wali_kelas` int(11) NOT NULL,
  PRIMARY KEY (`id_kelas`),
  KEY `id_datakelas` (`id_datakelas`),
  KEY `id_tahun_ajar` (`id_tahun_ajar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `master_kelas`
--

INSERT INTO `master_kelas` (`id_kelas`, `id_datakelas`, `id_tahun_ajar`, `wali_kelas`) VALUES
(1, 3, 1, 17),
(2, 4, 1, 0),
(3, 7, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_kelas_detail`
--

CREATE TABLE IF NOT EXISTS `master_kelas_detail` (
  `id_detailkelas` int(11) NOT NULL AUTO_INCREMENT,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  PRIMARY KEY (`id_detailkelas`),
  KEY `id_siswa` (`id_siswa`),
  KEY `id_kelas` (`id_kelas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `master_kelas_detail`
--

INSERT INTO `master_kelas_detail` (`id_detailkelas`, `id_kelas`, `id_siswa`) VALUES
(1, 1, 5),
(3, 1, 7),
(4, 1, 1),
(5, 1, 9),
(6, 1, 4),
(7, 1, 6),
(8, 1, 3),
(9, 1, 8),
(10, 1, 2),
(12, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `master_siswa`
--

CREATE TABLE IF NOT EXISTS `master_siswa` (
  `id_siswa` int(11) NOT NULL AUTO_INCREMENT,
  `nis` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jns_kelamin` enum('L','P') NOT NULL DEFAULT 'L',
  `id_agama` int(2) NOT NULL DEFAULT '1',
  `gol_darah` enum('Not Set','O','A','B','AB') NOT NULL,
  `alamat` varchar(300) NOT NULL COMMENT 'jalan/desa|rt|rw|norumah|kel|kec|kab/kota|prov',
  `telp` varchar(50) NOT NULL,
  `foto` varchar(150) NOT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `master_siswa`
--

INSERT INTO `master_siswa` (`id_siswa`, `nis`, `password`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jns_kelamin`, `id_agama`, `gol_darah`, `alamat`, `telp`, `foto`) VALUES
(1, '0088878228', '30efb9d9e15dc72f017ed8fb1eee1c61', 'Alridho Fatahilah', 'Jakarta', '2010-10-12', 'L', 1, 'Not Set', '|||||||', '', ''),
(2, '0097936011', '30efb9d9e15dc72f017ed8fb1eee1c61', 'Arinda Fataran', 'Bogor', '2010-10-12', 'P', 1, 'Not Set', '|||||||', '', ''),
(3, '0096526608', 'ded7b0d3163e874a83dd76e7bf2b1c12', 'Bimo Dwi Hatmojo', 'Sukabumi', '2010-02-01', 'L', 2, 'Not Set', '|||||||', '', ''),
(4, '0094676295', 'daef7a4dba12267de54eefdcba249fa4', 'Bunga Wijaya Kusuma', 'Jakarta', '2010-03-03', 'P', 2, 'Not Set', '|||||||', '', ''),
(5, '0082209863', 'daef7a4dba12267de54eefdcba249fa4', 'Chadijah Chatmiwati Santoso', 'Bogor', '2010-03-03', 'P', 1, 'Not Set', '|||||||', '', ''),
(6, '0096102012', '9a9b55c72853c476a86958594e048d92', 'Chesta Cavan', 'Jogjakarta', '2010-04-03', 'L', 2, 'Not Set', '|||||||', '', ''),
(7, '0086078989', '8b27163f8483ecc8eef31b12da6a313d', 'Cinta Yolanda Smeet''s', 'Bali', '2010-07-03', 'P', 2, 'Not Set', '|||||||', '', ''),
(8, '0097278527', '0a8f1ea4240a8e06cfdfee4b1a207e6a', 'Esa Ratih Eiqi Chairunnisa', 'Jakarta', '2010-12-03', 'P', 1, 'Not Set', '|||||||', '', ''),
(9, '0094447156', '4a021cd2dec0ff2bc99c697b5c0c3073', 'Ezzar Adheswara', 'Bandung', '2010-10-03', 'L', 2, 'Not Set', '|||||||', '', 'siswa_9.JPG'),
(10, '0084012123', 'daef7a4dba12267de54eefdcba249fa4', 'Fahdan Maulana Ihsan', 'Bogor', '2010-03-03', 'L', 1, 'Not Set', '|||||||', '', ''),
(11, '1212010', 'fbe81a6b47ed6b3c6a1d10df65959c9e', 'Fithrah Fauzan', 'Bogor', '1994-03-25', 'L', 1, 'O', '|||||||', '', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `akademik_jadwal`
--
ALTER TABLE `akademik_jadwal`
  ADD CONSTRAINT `akademik_jadwal_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `master_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `akademik_jadwal_detail`
--
ALTER TABLE `akademik_jadwal_detail`
  ADD CONSTRAINT `akademik_jadwal_detail_ibfk_1` FOREIGN KEY (`id_jadwal`) REFERENCES `akademik_jadwal` (`id_jadwal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `master_kelas`
--
ALTER TABLE `master_kelas`
  ADD CONSTRAINT `master_kelas_ibfk_1` FOREIGN KEY (`id_datakelas`) REFERENCES `app_kelas` (`id_datakelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `master_kelas_ibfk_2` FOREIGN KEY (`id_tahun_ajar`) REFERENCES `akademik_tahun_ajar` (`id_tahun_ajar`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `master_kelas_detail`
--
ALTER TABLE `master_kelas_detail`
  ADD CONSTRAINT `master_kelas_detail_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `master_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `master_kelas_detail_ibfk_2` FOREIGN KEY (`id_siswa`) REFERENCES `master_siswa` (`id_siswa`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
